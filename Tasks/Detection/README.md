

<!-- ## Annotation (Chen)
- Rédaction du guide d'annotation: définition de la catégorie "argument"
- Annotation de contributions (BRAT) -->


<!-- ## Classification (Mélanie, Pierre, Shuai) -->


[[_TOC_]]

# Utilsation

1. Installer les dépencances
```console
pip install -r requirements.txt
```

2. Configuration

Pour le paramétrage, rendez-vous dans app.py, fonction `extract_arg(xml_path, output="args.xml" ,format="xml", lim=None):`
- **xml_path** : le chemin du fichier xml
- **output** : le nom du fichier de sortie
- **format** : le format de sortie, **xml** ou **json**
- **lim** : le nombre de contributions à traiter

Il est également possible de modifier les règles à appliquer directement dans la définition fonction

3. Lancer l'application
```
python app.py
```

# Tâche : Détection


Nous proposons deux méthodologies :

1. Un système symbolique à partir de règles syntaxiques
2. Un système d'apprentissage à partir de CRF


# Méthode symbolique

## I. Utilisation

python

![](images/process.png)

## II. Description du traitement

### A. Prétraitements

Nous souhaitons détecter les arguments à partir de caractériques linguistiques (telles que les lemmes, pos, dépendances, etc).
Nous devons donc ajouter ces informations sur chaque contribution du corpus.

Nous choisissons l'outil **Stanza** qui permettra d'ajouter tout ce dont nous avons besoin.


<u>[Stanza](https://stanfordnlp.github.io/stanza/)</u>


> Stanza is a Python natural language analysis package. It contains tools, which can be used in a pipeline, to convert a string containing human language text into lists of sentences and words, to generate base forms of those words, their parts of speech and morphological features, to give a syntactic structure dependency parse, and to recognize named entities. The toolkit is designed to be parallel among more than 70 languages, using the Universal Dependencies formalism.

Voici la configuration utilisée :

```python
nlp = stanza.Pipeline('fr', processors="tokenize, mwt, pos, lemma, depparse", tokenize_no_ssplit=True)
```

**Remarque** : Le modèle utilisé est le modèle **français** par défaut entraîné sur le corpus français [GSD](https://universaldependencies.org/treebanks/fr_gsd/index.html).

Traitement | Explication
-|-
tokenize | Le texte est tokenizé
mwt | Les multimots sont tokenizés
pos | Annotation des catégories morphosyntaxiques
depparse | Annotation des des dépendances syntaxiques et des indices des dépendants
tokenize_no_ssplit | On désactive la segmentation des phrases pour travailler sur les contributions entières

Après l'annotation effectuée par Stanza nos phrases peuvent être représentées sous forme d'arbre syntaxique.




**Exemple 1**

<u>Contribution</u>

> Proportionnelle totale donc meilleure représentation des tendances des français.

<u>Informations linguistiques extraites</u>

index|form|lemma|pos|feats|head|dep
-|-|-|-|-|-|-
1|Proportionnelle|proportionnel|ADJ|Gender=Fem\|Number=Sing|0|root
2|totale|total|ADJ|Gender=Fem\|Number=Sing|1|amod
3|donc|donc|ADV|None|5|advmod
4|meilleure|meilleur|ADJ|Gender=Fem\|Number=Sing|5|amod
5|représentation|représentation|NOUN|Gender=Fem\|Number=Sing|1|conj
6|de|de|ADP|None|8|case
7|les|le|DET|Definite=Def\|Gender=Fem\|Number=Plur\|PronType=Art|8|det
8|tendances|tendance|NOUN|Gender=Fem\|Number=Plur|5|nmod
9|de|de|ADP|None|11|case
10|les|le|DET|Definite=Def\|Gender=Masc\|Number=Plur\|PronType=Art|11|det
11|français|français|NOUN|Gender=Masc\|Number=Plur|8|nmod
12|.|.|PUNCT|None|1|punct

<u>Représentation</u>


![](./images/tree1.png)


**Exemple 2**

<u>Contribution</u>

> L'idée du tirage au sort d'un certain nombre de députés parmi des citoyens lambda afin de limiter la sur représentativité de certaines catégories.

<u>Informations linguistiques extraites</u>

index|form|lemma|pos|feats|head|dep
-|-|-|-|-|-|-
1|L'|le|DET|Definite=Def\|Gender=Fem\|Number=Sing\|PronType=Art|2|det
2|idée|idée|NOUN|Gender=Fem\|Number=Sing|0|root
3|de|de|ADP|None|5|case
4|le|le|DET|Definite=Def\|Gender=Masc\|Number=Sing\|PronType=Art|5|det
5|tirage|tirage|NOUN|Gender=Masc\|Number=Sing|2|nmod
6|à|à|ADP|None|8|case
7|le|le|DET|Definite=Def\|Gender=Masc\|Number=Sing\|PronType=Art|8|det
8|sort|sort|NOUN|Gender=Masc\|Number=Sing|5|nmod
9|d'|de|ADP|None|12|case
10|un|un|DET|Definite=Ind\|Gender=Masc\|Number=Sing\|PronType=Art|12|det
11|certain|certain|ADJ|Gender=Masc\|Number=Sing|12|amod
12|nombre|nombre|NOUN|Gender=Masc\|Number=Sing|2|nmod
13|de|de|ADP|None|14|case
14|députés|député|NOUN|Gender=Masc\|Number=Plur|12|nmod
15|parmi|parmi|ADP|None|17|case
16|des|un|DET|Definite=Ind\|Gender=Masc\|Number=Plur\|PronType=Art|17|det
17|citoyens|citoyen|NOUN|Gender=Masc\|Number=Plur|12|nmod
18|lambda|lambda|ADJ|Gender=Masc\|Number=Sing|17|amod
19|afin|afin|ADV|None|12|advmod
20|de|de|ADP|None|21|mark
21|limiter|limiter|VERB|VerbForm=Inf|19|ccomp
22|la|le|DET|Definite=Def\|Gender=Fem\|Number=Sing\|PronType=Art|24|det
23|sur|sur|ADP|None|24|case
24|représentativité|représentativité|NOUN|Gender=Fem\|Number=Sing|21|obl
25|de|de|ADP|None|27|case
26|certaines|certain|DET|Gender=Fem\|Number=Plur|27|det
27|catégories|catégorie|NOUN|Gender=Fem\|Number=Plur|24|nmod
28|.|.|PUNCT|None|2|punct


<u>Représentation</u>

![](./images/tree2.png)


### B. Détection par règles

Pour extraire les propositions et les arguments nous écrivons des règles symboliques en python que nous appliquons sur chaque arbre.

Actuellement nous avons 11 règles:

*cliquez sur une règle pour voir les détails*

<details>
<summary>étant donné que</summary>

On détecte la séquence “étant donné  que”. La tête de l’argument est le gouverneur du token "étant" avec une relation “mark”. On suppose que la tête de la proposition est la racine ( relation “root” ) de la phrase.

Exemple

"D'autre part, vous l'aurez compris, le Consulat siégera dans le Sénat, étant donné que ces deux Chambres auront un pouvoir législatif en 'commun'."

Proposition : "D' autre part , vous l' aurez compris , le consulat siégera dans le sénat."
Argument : "ces deux chambres auront un pouvoir législatif en ' commun '."


</details>

<details>
<summary>parce que</summary>

On cherche à détecter la séquence “parce que”, ainsi que "parce qu'il faut". La tête de l’argument est le gouverneur du token “pour” avec une relation “mark”. On suppose que la tête de la proposition est la racine ( relation “root” ) de la phrase.

Exemple : parce que

"Une dernière chose, parce qu'il faut bien compter avec la culture pub, et parce que j'ai signé de mon nom cette contribution qui met le travail lui-même au rencart. Je tiens donc à dire que je ne suis pas le 'Lambert' qui A OUBLIE DE RENTRER DE VACANCES."


Proposition : "Une dernière chose , parce qu' il faut bien compter avec la culture pub je tiens donc à dire que je ne suis pas le ' lambert ' qui a oublie de rentrer de vacances."
Argument : "Et j' ai signé de mon nom cette contribution qui met le travail lui-même au rencart."

Exemple : parce qu'il faut

"Une dernière chose, parce qu'il faut bien compter avec la culture pub, et parce que j'ai signé de mon nom cette contribution qui met le travail lui-même au rencart. Je tiens donc à dire que je ne suis pas le 'Lambert' qui A OUBLIE DE RENTRER DE VACANCES."

Proposition : "Une dernière chose je tiens donc à dire que je ne suis pas le ' lambert ' qui a oublie de rentrer de vacances."
Argument : "Il faut bien compter avec la culture pub , et parce que j' ai signé de mon nom cette contribution qui met le travail lui-même au rencart."

</details>

<details>
<summary>même si</summary>
On n’utilise pas les arbres syntaxiques, on sectionne la phrase à partir du connecteur logique. On détecte la séquence “afin que” ou “afin de”.  On  considère que la partie de la phrase qui se trouve avant cette séquence est la proposition, et celle qui se trouve après l’argument.  

Exemple

"Sévérité sur Les évasions et fraudes fiscale. Et mise en place de l'impôts sur la nationalité française, comme ça même si les riches partent, ils sont obligés de cotiser en France."

Proposition: "Sévérité sur Les évasions et fraudes fiscale. Et mise en place de l'impôts sur la nationalité française, comme ça"
Argument: "les riches partent, ils sont obligés de cotiser en France."

</details>

<details>
<summary>anti prop</summary>
Cette règle tente une approche différente des autres.  Le but est de reconnaître les phrases qui ont beaucoup de chances d’être des propositions d’après leur structure grammaticale. On part du principe que si une phrase n’est pas une proposition, il s’agira probablement d’un argument. En effet, souvent la première partie de la contribution est souvent la réponse directe à la question posée (proposition). La suite justifie et explique cette réponse (argument). La schéma est parfois inversé, mais les propositions se ressemblent souvent. Pour les reconnaître on  vérifie si la phrase est:

- à l’impératif:
exemple: "Arretez de construire des dizaines de milliers de ronds points sur nos routes et de multiplier à outrance les panneaux de circulation. Ca coute trés cher et c'est pas vital.", argument: "Ca coute trés cher et c' est pas vital."  

- comporte une tournure impérative (il faut, on doit…):
exemple : "Il faut penser aux Français laïques avant tout. Les religions séparent souvent les gens. Stop aux voiles islamiques et autres.... ça suffit !", argument:  "Les religions séparent souvent les gens."  

- est une proposition infinitive:
exemple:  "Les médias ont un impact négatif dans notre société. Faire qu'il y est plus d'émissions qui démontrent que notre pays à de réels valeurs et que nous pouvons tous vivre ensemble de façon hamonieuse.", argument:  "Les médias ont un impact négatif dans notre société.   

- commence par “que” ( continuation logique de questions ouvertes comme “que proposez-vous (...)?” ou “que pensez-vous(...)”):    
exemple: "Que le président arrete d'insulter les français. Ivrognes, illétrés, fainéants, criminels contre l'humanité, lépreux, gens qui ne sont rien, etc, çs SUFFIT.", argument: "Ivrognes , illétrés , fainéants , criminels contre l' humanité , lépreux , gens qui ne sont rien , etc , çs suffit."  

Si c’est le cas, on continue la boucle, considérant qu’il s’agit d’une proposition. Une fois l’argument détecté, nous utilisons les arbres syntaxiques pour reconstituer la phrase. La règle ne renvoie pas les propositions, conçue dans un premier temps pour extraire uniquement les arguments.

</details>


<details>
<summary>afin que/de</summary>
On n’utilise pas les arbres syntaxiques, on sectionne la phrase à partir du connecteur logique. On détecte la séquence “afin que” ou “afin de”.  On  considère que la partie de la phrase qui se trouve avant cette séquence est la proposition, et celle qui se trouve après l’argument.  

Exemple

"Un formulaire de révocation des élus devrait etre envoyé tous les ans dans nos boîtes aux lettres afin de le révoquer si plus de 25% des questionnaires retournés le demande.”

Proposition: "Un formulaire de révocation des élus devrait etre envoyé tous les ans dans nos boîtes aux lettres."
Argument: "Le révoquer si plus de 25 % des questionnaires retournés le demande."
</details>


<details>
<summary>faits</summary>
Cette règle part du principe que l’utilisation de données factuelles, comme les pourcentages ou les chiffres relève d’une démarche argumentative, et permettrait donc de reconnaître des portions de contribution contenant des arguments.
Après application de la règle, on observe bon nombre de propositions contiennent aussi ces éléments (propositions très concrètes sur la parité ou les variations de salaire par exemple …). Une piste pour résoudre ce problème serait d’ajouter un filtre semblable à celui utilisé pour la règle “anti proposition” (ci-dessous) pour ne garder que les arguments contenant des données factuelles.

</details>

<details>
<summary>par consequent</summary>
On cherche d’abord à détecter la séquence “par conséquent". La tête de l’argument est le dépendant du token “par”. On suppose que la tête de la proposition est la racine ( relation “root” ) de la phrase.

Exemple

"Il faut baisser la CSG qui réduit les salaires, augmente le prix des loyers et bride par conséquent l'entreprenariat et la croissance."

Proposition: ""Il faut baisser la csg qui réduit les salaires , augmente le prix des loyers."
Argument: "Et bride par conséquent l'entreprenariat et la croissance."
</details>

<details>
<summary>donc</summary>


On détecte un token “donc”. La tête de l’argument est son gouverneur avec une relation “advmod”. La tête de la proposition à une relation “conj” avec la tête de l’argument.

Exemple

"Proportionnelle totale donc meilleure représentation des tendances des français."

![](images/tree_donc.png)

Proposition : "Proportionnelle totale."
Argument : "Meilleure représentation des tendances des français."

</details>

<details>
<summary>car</summary>

On détecte un token “car”. La tête de l’argument est son gouverneur avec une relation “cc”. La tête de la proposition est le gouverneur de l’argument avec une relation “conj”.

Exemple

"Il faut qu'on insiste plus sur les devoirs des citoyens car on se focalise trop sur nos seuls droits."

![](images/tree_car.png)

Proposition : “Qu'on insiste plus sur les devoirs des citoyens.”
Argument : “On se focalise trop sur nos seuls droits."


</details>


<details>
<summary>pour que</summary>

On cherche d’abord à détecter la séquence “pour que”. La tête de l’argument est le gouverneur du token “pour” avec une relation “mark”. On suppose que la tête de la proposition est la racine ( relation “root” ) de la phrase.

Exemple

“Mieux adapter nos lois électorales, pour que chaque élus ait une représentativité indiscutable."

![](images/tree_pour_que.png)


Proposition : "Mieux adapter nos lois électorales.",
Argument : "Pour que chaque élus ait une représentativité indiscutable."


</details>

<details>
<summary>par exemple</summary>

On cherche d’abord à détecter la séquence “par exemple”. La tête de l’argument est le dépendant du token “par”. On suppose que la tête de la proposition est la racine ( relation “root” ) de la phrase.

Exemple

"Qe tout ne soit pas décidé de PARIS comme par exemple la limitation de vitesse à 80 km/heure."

![](images/tree_par_exemple.png)

Proposition : "Qe tout ne soit pas décidé de paris.",
Argument : "Comme par exemple la limitation de vitesse à 80 km/heure."

</details>

<details>
<summary>eu égard à</summary>

Nous détectons d’abord la séquence “eu égard”. Ensuite, nous prenons en compte plusieurs situations : “à”, “au”, “aux” (lemme : à) et d’autres formes parfois erronées telles que “en”, “de” comme préposition ou tout simplement rien après cette séquence, qui expriment toutes néanmoins le même sens.
Le connecteur “eu égard à” est généralement suivi par la raison qui justifie la proposition avant et qui constitue donc un argument.

Exemple

“Il faut aussi fixer un plafond pour l'ensemble des impôts prélevés- national et local - eu égard au revenu de l'intéressé.”

Proposition : "Il faut aussi fixer un plafond pour l' ensemble des impôts prélevés - national et local."
Argument : "Le revenu de l'intéressé."

</details>

<details>
<summary>d'où</summary>

Il nous revient tout de suite que la locution adverbiale “d’où” renvoie aux références de lieu mais cela peut également s’utiliser pour exprimer la conséquence. En ce qui nous concerne, c’est bien le dernier qui est intéressant. La partie à gauche du connecteur est l’argument et celle à droite la proposition.
On a utilisé la structure “d’où + déterminant” pour exclure le premier usage alors que l’on a observé que cette règle est en grand partie vraie et entraîne des fois des erreurs. On n’a pas pu en faire une règle absolue.

Exemple

"Seuls la moitié des Français paient l'impôt sur le revenu. L'assiette est trop faible, d'où notre proposition."

Proposition : "Notre proposition."
Argument : "Seuls la moitié des français paient l'impôt sur le revenu . l'assiette est trop faible."

</details>

---

# Méthode par apprentissage (Wapiti-Limsi)

**Voir le document rapport.pdf**

Ci-dessous les grandes étapes réalisées :
- simplification du guide d'annotation (BRAT)
- transformation du format .ann en tabulaire
- enrichissement des fichiers de configuration à partir de constats linguistiques
- paramétrage et entraînement du modèle
- évaluation des annotations produites
