# ====== REGLES DE SHUAI ======
def eu_egard_a(tree):
    """
    PROP - Connecteur - ARG
    """

    for word in tree.words:
        if word.text == "eu" and tree.words[int(word.id)].text == "égard":

            if tree.words[int(word.id)+1].lemma == "à":
                # Début du connecteur
                id_connect_head = int(word.id)
                # Fin du connecteur
                id_connect_tail = int(word.id)+2

                # Proposition avant le connecteur
                prop = [word.text for word in tree.words[:id_connect_head-1]]
                phrase_prop = form_sentence(" ".join(prop))

                # Argument après le connecteur
                arg = [word.text for word in tree.words[id_connect_tail:]]
                phrase_arg = form_sentence(" ".join(arg))

            # Gestion des formes erronnées : eu égard 'en', eu égard 'de', eu égard ' ', etc.
            else:
                id_connect_head = int(word.id)
                # Connecteur moins un token
                id_connect_tail = int(word.id)+1

                prop = [word.text for word in tree.words[:id_connect_head-1]]
                phrase_prop = form_sentence(" ".join(prop))

                arg = [word.text for word in tree.words[id_connect_tail:]]
                phrase_arg = form_sentence(" ".join(arg))

            return phrase_prop, phrase_arg
    return None, None

def d_ou(tree):
    """
    ARG - Connecteur - PROP
    """

    for word in tree.words:
        # Exclusion des références de lieu :
        # "d'où" s'utilise comme locution adverbiale (pas comme référence de lieu) au début
        # d'un groupe nominal qui commence toujours par un déterminant
        if word.text == "d\'" and tree.words[int(word.id)].lemma == "où" and tree.words[int(word.id)+1].upos == "DET":
            # Début du connecteur
            id_connect_head = int(word.id)

            # Proposition après le connecteur
            prop = [word.text for word in tree.words[id_connect_head+1:]]
            phrase_prop = form_sentence(" ".join(prop))

            # Argument avant le connecteur
            arg = [word.text for word in tree.words[:id_connect_head-1]]
            phrase_arg = form_sentence(" ".join(arg))

            return phrase_prop, phrase_arg
    return None, None


# ====== REGLES DE CHEN =====
def etant_donne_que(tree):
    for word in tree.words :

        if word.text == "étant" and word.deprel == "mark" and tree.words[int(word.id)].text == "donné" and tree.words[int(word.id)+1].lemma == "que":
            # print(tree.text)

            # --- CONNECTEUR ---
            # print("connecteur : étant donnée que")
            head_connect = tree.words[int(word.id) -1]
            id_head_connect = int(head_connect.id)

            # tête d'argument
            head_arg = tree.words[word.head - 1]
            for w in tree.words :
                if w.deprel == "root":
                    head_prop = w
                    break
            id_head_arg = int(head_arg.id)
            id_head_prop = int(head_prop.id)

            # --- PROPOSITION ---
            # la tête du segment
            head_list = [[id_head_prop]]
            #id de la tête de la proposition à exclure
            exclusions = [id_head_arg]
            # le segment propsition reformaté en phrase
            prop = get_segment(head_list, tree, exclusions)
            # print("prop :", prop)

            # --- ARGUMENT ---
            # la tête du segment
            head_list = [[id_head_arg]]
            #pour enlever "parce que" dans le résultat final
            exclusions = [id_head_connect]

            arg = get_segment(head_list, tree, exclusions)
            # enlever le "que"
            arg = arg[4:]
            # print("arg :", arg)

            # print("======")
            return prop, arg

    return None, None


def parce_que(tree):
    for word in tree.words :

        if word.text == "parce" and word.deprel == "mark" and tree.words[int(word.id)].lemma == "que":
            # print(tree.text)

            # --- CONNECTEUR ---
            # print("connecteur : parce que")
            head_connect = tree.words[int(word.id) -1]
            id_head_connect = int(head_connect.id)

            # tête d'argument
            head_arg = tree.words[word.head - 1]
            for w in tree.words :
                if w.deprel == "root":
                    head_prop = w
                    break
            id_head_arg = int(head_arg.id)
            id_head_prop = int(head_prop.id)

            # --- PROPOSITION ---
            # la tête du segment
            head_list = [[id_head_prop]]
            #id de la tête de la proposition à exclure
            exclusions = [id_head_arg]
            # le segment propsition reformaté en phrase
            prop = get_segment(head_list, tree, exclusions)
            # print("prop :", prop)

            # --- ARGUMENT ---
            # la tête du segment
            head_list = [[id_head_arg]]
            #pour enlever "parce que" dans le résultat final
            exclusions = [id_head_connect]

            arg = get_segment(head_list, tree, exclusions)
            # print("arg :", arg)

            # print("======")
            return prop, arg

        # le cas de "parce qu'il faut", mais il va générer deux analyses séparées s'ils apparaissent en même temps dans une phrase.
        if word.text == "parce" and word.deprel == "mark" and tree.words[int(word.id)].text == "qu\'" and tree.words[word.head-1].text =="faut":

            # --- connecteur ---
            # print("connecteur : parce qu'il faut")
            head_connect = tree.words[int(word.id)-1]
            id_head_connect = int(head_connect.id)

            # tête d'argument
            head_arg = tree.words[word.head - 1]
            for w in tree.words :
                if w.deprel == "root":
                    head_prop = w
                    break
            id_head_arg = int(head_arg.id)
            id_head_prop = int(head_prop.id)

            # --- PROPOSITION ---
            # la tête du segment
            head_list = [[id_head_prop]]
            #id de la tête de la proposition à exclure
            exclusions = [id_head_arg]
            # le segment propsition reformaté en phrase
            prop = get_segment(head_list, tree, exclusions)
            # print("prop :", prop)

            # --- ARGUMENT ---
            # la tête du segment
            head_list = [[id_head_arg]]
            #pour enlever "parce que" dans le résultat final
            exclusions = [id_head_connect]

            arg = get_segment(head_list, tree, exclusions)
            # print("arg :", arg)

            # print("======")
            return prop, arg
    return None, None


# ======== MELANIE =========================================

def meme_si(tree):
    prop = None
    arg = None
    voc = ["mème si", "même si", "méme si", "meme si"]
    voc2 = ["mème", "même", "méme", "meme"]
    if any(term in tree.text for term in voc):
        #print(tree.text)
        for word in tree.words:
            try:
                if word.lemma in voc2 and tree.words[int(word.id)].lemma == "si":
                    id_connect = int(word.id)
                    prop = tree.words[:id_connect-1]
                    prop = [word.text for word in prop]
                    phrase_prop = form_sentence(" ".join(prop))
                    # print(f"avant connecteur = proposition: {phrase_prop}")
                    arg = tree.words[id_connect+1:]
                    arg = [word.text for word in arg]
                    phrase_arg = form_sentence(" ".join(arg))
                    # print(f"après connecteur = argument: {phrase_arg}")
                    return phrase_prop, phrase_arg
            except IndexError:
                continue
    return prop, arg


def anti_prop(tree):
    """
    APPROCHE INVERSÉÉ: éliminer les phrases qui sont des propositions (la majorité!) + format peu rédigé donc peu de connecteurs logiques
    garde les phrases contenant des auxiliaires au mode indicatif != tournures impersonnelles ou averbales des propositions
    version avec beaucoup de résultats mais pas très affinés: tous les verbes conjugués
    version avec moins de résultats mais correspondant souvent à des arguments: auxiliaire conjugué
    """
    arg = None
    prop = None
    id_points = []
    sents = []

    for word in tree.words:
        if word.text == ".":
            id_points.append(int(word.id))
    if len(id_points) == 0 or (len(id_points) == 1 and id_points[0] == tree.words[-1].id):
        multiphrase = False
    else:
        multiphrase = True

    if multiphrase:
        try:
            if len(id_points) == 1:
                sent1 = tree.words[:id_points[0]]
                sent2 = tree.words[id_points[0]:]
                sents = [sent1, sent2]
            elif len(id_points) == 2:
                sent1 = tree.words[:id_points[0]]
                sent2 = tree.words[id_points[0]:id_points[1]]
                sent3 = tree.words[id_points[1]:]
                sents = [sent1, sent2, sent3]
            for sent in sents:
                for word in sent:
                    if word.id == "1" and (word.upos == "VERB" or word.upos == "AUX") and ("VerbForm=Inf" in word.feats or "Mood=Imp" in word.feats):
                        break
                    elif word.id == "1" and word.lemma == "que":
                        break
                    elif word.id == "1" and word.upos == "NOUN":
                        break
                    elif (word.upos == "AUX" or word.upos == "VERB") and ("Mood=Ind" in word.feats or "Mood=Cnd" in word.feats):
                        if word.lemma == "falloir" or word.lemma == "devoir":
                            break
                        segment = [word.text for word in sent]
                        arg = form_sentence(" ".join(segment))
                        # print(f"arg: {segment} ")
                        # print("======")
                        # break
                        return prop, arg
        except TypeError:
            pass
    else:
        try:
            if (tree.words[0].upos == "VERB" or tree.words[0].upos == "AUX") and ("VerbForm=Inf" in tree.words[0].feats or "Mood=Imp" in tree.words[0].feats):
                pass
            elif tree.words[0].lemma == "que":
                pass
            elif word.id == "1" and word.upos == "NOUN":
                pass
            for word in tree.words:
                if (word.upos == "AUX" or word.upos == "VERB") and ("Mood=Ind" in word.feats or "Mood=Cnd" in word.feats):
                    if word.lemma == "falloir" or word.lemma == "devoir":
                        break
                    for w in tree.words:
                        if w.deprel == "root":
                            head_arg = w
                            break
                    id_head_arg = int(head_arg.id)
                    # --- argument ---
                    # la tête du segment
                    head_list = [[id_head_arg]]
                    # le segment argument reformaté en phrase
                    arg = get_segment(head_list, tree)
                    # print("arg :", arg)
                    # print("======")
                    return prop, arg
        except TypeError:
            pass
    return prop, arg


def afin_que_de(tree):
    for word in tree.words:
        if (word.text == "afin" and word.deprel == "advmod" and tree.words[int(word.id)].lemma == "que") or ((word.text == "afin" and word.deprel == "advmod" and tree.words[int(word.id)].lemma == "de")):
            # print(tree.text)
            # print("connecteur : afin que/de")
            # exclut d'autres phrases
            id_exclusion = [int(w.id) for w in tree.words if w.text == "."]
            if id_exclusion != []:
                id_exclusion = id_exclusion[0]
            id_exclusion = int(tree.words[-1].id)
            id_connect = [int(w.id) for w in tree.words if w.text == "afin"]
            id_connect = id_connect[0]
            prop = tree.words[:id_connect -1]
            prop = [word.text for word in prop]
            phrase_prop = form_sentence(" ".join(prop))
            # print(f"avant connecteur = proposition: {phrase_prop}")
            arg = tree.words[id_connect+1:id_exclusion]
            arg = [word.text for word in arg]
            phrase_arg = form_sentence(" ".join(arg))
            # print(f"après connecteur = argument: {phrase_arg}")
            # print("======")
            return phrase_prop, phrase_arg
    return None, None


# problème: beaucoup de propositions! utiliser anti prop pour affiner
def faits(tree):
    indices = ["pourcentage", "%", "pourcent", "pour cent", "statistique", "stats", "statistiques", "sondage"]
    for word in tree.words:
        if any(term in word.text for term in indices):
            for w in tree.words :
                if w.deprel == "root":
                    head_prop = w
                    break
            id_head_arg = int(head_prop.id)
            head_list = [[id_head_arg]]
            arg = get_segment(head_list, tree)
            return None, arg
            # print("arg :", arg)
            # print("======")


def par_consequent(tree):
    if "par conséquent" in tree.text:
        for word in tree.words:

            if word.deprel == "fixed" and tree.words[int(word.head) -1].text == "par" :
                head_connect = tree.words[int(word.head) -1]
                head_arg = tree.words[head_connect.head - 1]
                id_head_arg = int(head_arg.id)
                for w in tree.words :
                    if w.deprel == "root":
                        # head_prop = w
                        head_prop = w
                        break
                id_head_prop = int(head_prop.id)
                # --- PROPOSITION ---
                # la tête du segment
                head_list = [[id_head_prop]]
                # les id à exclure : id de la tête de la proposition
                exclusions = [id_head_arg]
                # le segment propsition reformaté en phrase
                prop = get_segment(head_list, tree, exclusions)
                # print("prop :", prop)

                # --- ARGUMENT ---
                # la tête du segment
                head_list = [[id_head_arg]]
                # segment arg reformaté en phrase
                arg = get_segment(head_list, tree)
                # print("arg :", arg)
                # print("======")
                # break
                return prop, arg
    return None, None

# =============== REGLES DE PIERRE QUI MARCHENT ================================


def par_exemple(tree):
    """
    "par" -[fixed]-> "exemple"
    "par" --> ARG (normalemnt la dépencance est advmod mais à confirmer)
    deparse:root --> PROP (plus simple mais sans doute possible d'être plus fin)
    """
    if "par exemple" in tree.text :
        for word in tree.words:

            if word.deprel == "fixed" and tree.words[int(word.head) -1].text == "par" :

                # --- CONNECTEUR ---
                # id du connecteur : par (exemple)
                head_connect = tree.words[int(word.head) -1]
                # id_head_connect = int(head_connect.id)
                # print("connecteur : par exemple")

                # Recherche des têtes des segments
                head_arg = tree.words[head_connect.head - 1]
                id_head_arg = int(head_arg.id)
                for w in tree.words :
                    if w.deprel == "root":
                        # head_prop = w
                        head_prop = w
                        break
                id_head_prop = int(head_prop.id)

                # --- PROPOSITION ---
                # la tête du segment
                head_list = [[id_head_prop]]
                # les id à exclure : id de la tête de la proposition
                exclusions = [id_head_arg]
                # le segment propsition reformaté en phrase
                prop = get_segment(head_list, tree, exclusions)
                # print("prop :", prop)

                # --- ARGUMENT ---
                # la tête du segment
                head_list = [[id_head_arg]]
                # segment arg reformaté en phrase
                arg = get_segment(head_list, tree)
                # print("arg :", arg)
                # print("======")
                # break
                return prop, arg

    return None, None

def car(tree):
    """
    PROP -[conj]-> ARG
    ARG -[cc]-> "car"
    """
    for word in tree.words:

        if word.lemma == "car" and word.upos == "CCONJ":

            # repr_tree(tree)

            # print(tree.text)
            # --- CONNECTEUR ---
            # id du connecteur : car
            id_head_connect = int(word.id)
            # print("connecteur : car")

            # Recherche des têtes des segments
            head_arg = tree.words[word.head - 1]
            head_prop = tree.words[head_arg.head - 1]
            id_head_arg = int(head_arg.id)
            id_head_prop = int(head_prop.id)

            # --- PROPOSITION ---
            # la tête du segment
            head_list = [[id_head_prop]]
            # les id à exclure : id de la tête de la proposition
            exclusions = [id_head_arg]
            # le segment propsition reformaté en phrase
            prop = get_segment(head_list, tree, exclusions)
            # print("prop :", prop)

            # --- ARGUMENT ---
            # la tête du segment
            head_list = [[id_head_arg]]
            # les id à exclure : id du connecteur
            exclusions = [id_head_connect]
            # segment arg reformaté en phrase
            arg = get_segment(head_list, tree, exclusions)
            # print("arg :", arg)

            # print("======")
            # break
            return prop, arg

    return None, None



def donc(tree):
    """
    ARG -[conj]- PROP  ( Sens de la dépendance variable )
    ARG -[advmod]-> "donc"
    """
    for word in tree.words:

        if word.lemma == "donc" and word.deprel == "advmod":
            # print(tree.text)

            # --- CONNECTEUR ---
            id_head_connect = int(word.id)
            # print("connecteur : donc")

            # Recherce des têtes des segments
            head_arg = tree.words[word.head - 1]
            id_head_arg = int(head_arg.id)


            # Gère le sens de la dépendance :
            # - arg -[conj]-> prop
            for w in tree.words :
                if w.deprel == "conj" and w.head == id_head_arg:
                    id_head_prop = int(w.id)
                    break
            # - prop -[conj]-> arg
            else :
                head_prop = tree.words[head_arg.head - 1]
                id_head_prop = int(head_prop.id)

            # --- PROPOSITION ---
            # la tête du segment
            head_list = [[id_head_prop]]
            # les id à exclure : id de l'arg
            exclusions = [id_head_arg]
            # segment prop reformaté en phrase
            prop = get_segment(head_list, tree, exclusions)
            # print("prop :", prop)

            # --- ARGUMENT ---
            # la tête du segment
            head_list = [[id_head_arg]]
            # les id à exclure : id du connecteur, id de la prop
            exclusions = [id_head_connect, id_head_prop]
            # segment arg reformaté en phrase
            arg = get_segment(head_list, tree, exclusions)
            # print("arg :", arg)
            # print("======")
            # break
            return prop, arg
    return None, None



def pour_que(tree):
    """
    ARG -[mark]-> "pour"
    "pour" < "que"
    deparse:root --> PROP (plus simple mais sans doute possible d'être plus fin)
    """
    for word in tree.words :
        if word.text == "pour" and word.deprel == "mark" and tree.words[int(word.id)].text == "que":
            # print(tree.text)
            # --- CONNECTEUR ---
            # id_head_connect = int(word.id)
            # print("connecteur : pour que")

            # Recherce des têtes des segments
            head_arg = tree.words[word.head - 1]
            # head_prop = tree.words[head_arg.head - 1]
            for w in tree.words :
                if w.deprel == "root":
                    head_prop = w
                    break
            id_head_arg = int(head_arg.id)
            id_head_prop = int(head_prop.id)

            # --- PROPOSITION ---
            # la tête du segment
            head_list = [[id_head_prop]]
            # les id à exclure : id de la tête de la proposition
            exclusions = [id_head_arg]
            # le segment propsition reformaté en phrase
            prop = get_segment(head_list, tree, exclusions)
            # print("prop :", prop)

            # --- ARGUMENT ---
            # la tête du segment
            head_list = [[id_head_arg]]
            # segment arg reformaté en phrase
            arg = get_segment(head_list, tree)
            # print("arg :", arg)
            # print("======")
            # break
            return prop, arg
    return None, None





def get_segment(head_list, tree, exclusions=[]):
    """
    Permet d'obtenir une séquence d'un sous-arbre à partir d'un indice. (= tête de la séquence)

    Args:
        head_list    : liste qui contient l'indice de la tête du sous-abre
        tree         : l'arbre sur lequel extraire la séquance
        exlusions    : liste contenant les indices des branches à exclure
    """
    sub_tree = []

    for w in tree.words:
        if int(w.head) in head_list[-1] and int(w.id) not in exclusions:
            sub_tree.append(int(w.id))
    if len(sub_tree) == 0:
        head_list = [n for l in head_list for n in l]
        segment = [tree.words[id-1].text for id in sorted(head_list)]
        segment = " ".join(segment)

        # On nettoie le segment, s'il est vide on renvoie None
        try :
            segment = form_sentence(segment)
            return segment
        except :
            return None

    head_list.append(sub_tree)
    return get_segment( head_list, tree, exclusions )


def form_sentence(text):
    """Nettoie et reforme une phrase correcte"""

    # print("N",text)
    text = text.replace("(", "")
    text = text.replace(")", "")
    text = text.strip("., -")
    text = text.replace("de les", "des")
    text = text.replace("de le", "du")
    text = text.replace("à le", "au")
    text = text.replace("à les", "aux")

    text = text.capitalize()
    if text[-1] not in ".!?":
        text += "."
    return text


def repr_tree( tree ):
    """Affiche une représentation conllu de l'arbre"""

    print("---")
    print("# text = ", tree.text)
    for word in tree.words:
        print(f"{word.id}\t{word.text}\t{word.lemma}\t{word.upos}\t_\t{word.feats}\t{word.head}\t{word.deprel}\t_\t_")


def apply(tree, *rules):
    """Applique les règles"""
    extracted = []
    for rule_name in rules:
        if rule_name == "car" : prop, arg = car(tree); rule = "car"
        if rule_name == "donc" : prop, arg = donc(tree); rule = "donc"
        if rule_name == "par exemple" : prop, arg = par_exemple(tree); rule = "par exemple"
        if rule_name == "pour que" : prop, arg = pour_que(tree); rule = "pour que"
        if rule_name == "par conséquent" : prop, arg = par_consequent(tree); rule = "par conséquent"
        # if rule_name == "faits" : prop, arg = faits(tree): rule  = "faits"
        if rule_name == "afin de/que" : prop, arg = afin_que_de(tree); rule = "afin de/que"
        if rule_name == "meme si": prop, arg = meme_si(tree); rule = "meme si"
        if rule_name == "anti prop" : prop, arg = anti_prop(tree); rule = "anti prop"
        if rule_name == "parce que" : prop, arg = parce_que(tree); rule = "parce que"
        if rule_name == "étant donné que" : prop, arg = etant_donne_que(tree); rule = "étant donné que"
        if rule_name == "eu égard à" : prop, arg = eu_egard_a(tree); rule ="eu égard à"
        if rule_name == "d'où" : prop, arg = d_ou(tree); rule ="d'où"
        if arg is not None:
            extracted.append((rule, prop, arg))
    return extracted
