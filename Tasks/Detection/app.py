
import stanza
import rules
import xml.etree.ElementTree as ET
import random
from tqdm import tqdm
import json

def extract_arg(xml_path, output="args.xml" ,format="xml", lim=None):

    Xtree = ET.parse(xml_path)
    root = Xtree.getroot()

    if lim == None:
        lim = len(root)

    if format == "xml":

        for i in tqdm(range(lim)):
            child = root[i]
            if len(child.text) < 200:
                doc = nlp(child.text)
                for sent in doc.sentences:

                    extracted = rules.apply(
                        sent,
                        "car",
                        "donc",
                        "par exemple",
                        "pour que",
                        "par conséquent",
                        "afin de/que",
                        "anti prop",
                        "meme si",
                        "parce que",
                        "étant donné que",
                        "eu égard à",
                        "d'où")

                    if len(extracted) != 0:
                        detected = ET.SubElement(child, "detected")
                        for i, db in enumerate(extracted):
                            #print(db)
                            doublet = ET.SubElement(detected, "doublet")
                            ET.SubElement(doublet, "prop").text = db[0]
                            ET.SubElement(doublet, "arg").text = db[1]

        Xtree.write(output)

    if format == "json":

        corpus = []

        for i in tqdm(range(lim)):
            child = root[i]
            if len(child.text) < 200:
                doc = nlp(child.text)
                for sent in doc.sentences:
                    # rules.repr_tree(sent)
                    extracted = rules.apply(
                        sent,
                        "car",
                        "donc",
                        "par exemple",
                        "pour que",
                        "par conséquent",
                        "afin de/que",
                        "meme si",
                        "anti prop",
                        "parce que",
                        "étant donné que",
                        "eu égard à",
                        "d'où")

                    # data = child.attrib
                    # data["text"] = child.text

                    if len(extracted) != 0:

                        data = child.attrib
                        data["text"] = child.text
                        detected = []
                        for _, db in enumerate(extracted):
                            detected.append(
                                {
                                    "rule":db[0],
                                    "prop":db[1],
                                    "arg":db[2]
                                })

                        data["detected"] = detected

                        corpus.append(data)

        with open(output, "w", encoding="utf8") as json_file:
            json.dump(corpus, json_file, indent=4, ensure_ascii=False)


if __name__ == "__main__":

    # décommenter si première utilisation de Stanza
    # stanza.download('fr')

    nlp = stanza.Pipeline('fr', processors="tokenize, mwt, pos, lemma, depparse", tokenize_no_ssplit=True)

    # xml_path = "./echantillon.xml"
    xml_path = "../Restructuration/GDN_struct.xml"
    extract_arg(xml_path, "args_test.json", format="json",lim=100)

    # === tests Pierre ===
    # text1 = "Il faut réduire les salaires des ministres car ils sont trop payés pour leur travail."
    # text2 = "Il est malade donc tu dois prendre soin de lui"
    # corpus = [text1, text2]

    # === tests Mélanie ===
    # test_text = "je veux que l'on mécoute, par conséquent je parlerai plus fort!"
    # test_text2 = "il voulait changer, si bien qu'il créa un parti"
    # test_text3 = "la démocratie est conçue de telle façon que les deux solutionq soient violentes"
    # test_text4 = "nous devons repenser le monde de telle sorte que les inégalités disparaissent"
    # test_text5 = "je me sens opprimée par l'état, de sorte que je refuse d'aller voter"
    # test_text6 = "80% des habitants des villes souhaitent déménager, par conséquent nous devons repenser notre urbanisme"
    # test_text7 = "Nous devons augmenter les impôts sur la fortune afin de garantir des services publics"
    # test_text8 = "Nous devons augmenter les impôts sur la fortune afin que l'on garantisse des services publics"
    # test_text9 = "Avoir envie de fermer les frontières est un réflexe primitif."
    # test_text10 = "Suppression de l'ISF"
    # test_text11 = "Arrêtons de tourner autour du pot"
    # test_text12 = "Macron démission"
    # test_text13 = "Il faudrait se demander ce qui ne va pas"
    # corpus = [test_text13]

    # ====== tests Chen =====
    # text0 = "Une dernière chose, parce qu'il faut bien compter avec la culture pub, et parce que j'ai signé de mon nom cette contribution qui met le travail lui-même au rencart. Je tiens donc à dire que je ne suis pas le 'Lambert' qui A OUBLIE DE RENTRER DE VACANCES."
    # text1 = "Sinon nous irons tout droit à la débâcle parce que nous avons perdu notre âme."
    # text2 = "Car il faut bien être initié pour se retrouver dans ce maquis opaque auquel chaque législature ajoute des strates."
    # text3 = "D'autre part, vous l'aurez compris, le Consulat siégera dans le Sénat, étant donné que ces deux Chambres auront un pouvoir législatif en 'commun'."
    # text4 = "Et surtout, étant donné que Bercy fait remonter toutes les informations concernant les contribuables, grâce à de lourds et onéreux investissements informatiques, il serait opportun d'abandonner ce système 'déclaratif' qui est aussi insupportable que le paiement de l'ISF lui-même."
    # corpus = [text3,text4]

    # ====== Tests Shuai =======
    # text_shuai_1 = "Services des URGENCES en FRANCE qu'il est URGENT de réformer pour permettre une véritable prise en charge exclusive des URGENCES avec humanité et respect et sans prise de risque eu égard à la saturation actuelle de ces services indispensables pour tous; au delà des moyens exponentiels à mettre, je pense qu'une réforme globale de nos services de santé doit permettre une optimisation et une efficacité pour tous avec l'exigence du respect des personnes en demande de soins; Aujourd'hui nos services de santé sont une honte pour nous, comment au quotidien laisser les malades, vieillards dans des brancards et dans les couloirs en attentes des heures pour une prise en charge ! C'est juste pas normal et inacceptable pour notre pays en 2019. Merci."
    # text_shuai_2 = "Augmentation des traitements des enseignants de l'éducation nationale notamment du 1er degré, ces traitements sont honteux au regard du travail fourni et du comparatif général avec les pays européen ! Si rien n'est fait, déjà que peu sont attirés eu égard au montant des salaires du début à la fin de la carrière, nous allons nous retrouver avec une éducation nationale digne du Bantoustan…."
    # corpus = [text_shuai_1, text_shuai_2]