#! /usr/bin/python3.6
# coding: utf8

import xml.etree.ElementTree as ET
from tqdm import tqdm

def transform(xml_path, name="args.xml"):
    
    Xtree = ET.parse(xml_path)
    root = Xtree.getroot()
    for i in tqdm(range(len(root))):
        contrib = root[i]
        text = contrib.text
        contrib.text = ""
        fulltext = ET.SubElement(contrib, "fulltext")
        fulltext.text = text

    Xtree.write(name)


if __name__ == "__main__":
    PATH = "/home/rochet/Projets/argument-mining/inalco-tal-m2/Tasks/Restructuration/GDN_struct.xml"
    transform(xml_path=PATH)