TOUT
processed 4649 tokens with 167 phrases; found: 95 phrases; correct: 7.
accuracy:  43.21%; precision:   7.37%; recall:   4.19%; FB1:   5.34
         Argument: precision:   2.33%; recall:   1.56%; FB1:   1.87  43
         Marqueur: precision:   0.00%; recall:   0.00%; FB1:   0.00  0
      Proposition: precision:  11.54%; recall:   7.69%; FB1:   9.23  52
                p: precision:   0.00%; recall:   0.00%; FB1:   0.00  0

EXTERIEUR (mais identique, même fichier?)
processed 4649 tokens with 167 phrases; found: 95 phrases; correct: 7.
accuracy:  43.21%; precision:   7.37%; recall:   4.19%; FB1:   5.34
         Argument: precision:   2.33%; recall:   1.56%; FB1:   1.87  43
         Marqueur: precision:   0.00%; recall:   0.00%; FB1:   0.00  0
      Proposition: precision:  11.54%; recall:   7.69%; FB1:   9.23  52
                p: precision:   0.00%; recall:   0.00%; FB1:   0.00  0

SURFACE
processed 4649 tokens with 167 phrases; found: 89 phrases; correct: 10.
accuracy:  42.53%; precision:  11.24%; recall:   5.99%; FB1:   7.81
         Argument: precision:   7.41%; recall:   3.12%; FB1:   4.40  27
         Marqueur: precision:   0.00%; recall:   0.00%; FB1:   0.00  0
      Proposition: precision:  12.90%; recall:  10.26%; FB1:  11.43  62
                p: precision:   0.00%; recall:   0.00%; FB1:   0.00  0

PAR COEUR (marche mieux!)
processed 4649 tokens with 167 phrases; found: 77 phrases; correct: 6.
accuracy:  43.75%; precision:   7.79%; recall:   3.59%; FB1:   4.92
         Argument: precision:   2.38%; recall:   1.56%; FB1:   1.89  42
         Marqueur: precision:   0.00%; recall:   0.00%; FB1:   0.00  0
      Proposition: precision:  14.29%; recall:   6.41%; FB1:   8.85  35
                p: precision:   0.00%; recall:   0.00%; FB1:   0.00  0
