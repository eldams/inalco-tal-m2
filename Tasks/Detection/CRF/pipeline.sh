echo "--- Start pipeline"
echo "######## Training"
wapiti train -a rprop -p config/config-tt.tpl tab_GDN-train.BIO modele-rprop
wapiti train -a sgd-l1 -p config/config-tt.tpl tab_GDN-train.BIO modele-sgd
wapiti train -a l-bfgs -p config/config-tt.tpl tab_GDN-train.BIO modele-bfgs


echo "\n"
echo "######## Tagging test data"
wapiti label -m modele-rprop tab_GDN-test.BIO > ann_arg-rprop.txt
wapiti label -m modele-sgd tab_GDN-test.BIO > ann_arg-sgd.txt
wapiti label -m modele-bfgs tab_GDN-test.BIO > ann_arg-bfgs.txt
echo "\n"
echo "######## Log"
echo "######## LogRPROP" >> algos-resultat.txt
perl conlleval.pl -d '\t' < ann_arg-rprop.txt >> algos-resultat.txt
echo "######## LogSGD" >> algos-resultat.txt
perl conlleval.pl -d '\t' < ann_arg-sgd.txt >> algos-resultat.txt
echo "######## LogBFGS" >> algos-resultat.txt
perl conlleval.pl -d '\t' < ann_arg-bfgs.txt >> algos-resultat.txt
cat algos-resultat.txt
echo "--- Done"
