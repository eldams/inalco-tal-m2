# Prétraitement - Marqueurs
Maeva

## Entrée
Texte brut (voir Remarques Générales)

## Sortie 
Document structuré au format XML avec balises marqueurs

## Traitements opérés sur le corpus

* l'ajout de balises marqueurs grâce à Unitex Gramlab
* le remplacement des entitées XML sur les chevrons après le passage par le script fromRawtoXml.py de Lucie et Clémence -> grâce au script remetChevrons.py
* création d'un fichier statistiques pour les marqueurs

### Ajout de marqueurs

* il faut prendre le fichier GDN.txt et le passer dans Unitex Gramlab
* appliquer l'automate marqueurs du dossier Automates sur le texte
* produire le fichier GDN_marqueur.txt en sortie

L'automate marqueurs contient des liens vers d'autres automates, qui contiennent chacun des marqueurs appartenant à une catégorie (causalité, conclusion, explication...)
En appliquant cet automate, on applique tous les automates du dossier Automates.
On obtient donc un fichier txt contenant des balises <marqueur>

#### Passage par le script fromRawtoXml.py 

* passer le fichier GDN_marqueur.txt dans le script fromRawtoXml.py
* passer le fichier sortie GDN_marqueur_struct.xml dans le script remetChevrons.py avec la commande python3 remetChevrons.py > fichier_final_marqueurs.xml

On passe le fichier txt obtenu après l'étape précédente par le script fromRawtoXml.py pour le mettre au format XML. Pendant cette étape, les chevrons des balises marqueurs sont changés en entités XHTML, c'est donc pour cela que j'ai produit le script remetChevrons.py pour remettre les chevrons correctement.

### Création du fichier stats_marqueur.txt

* passer le fichier fichier_final_marqueurs.xml dans le script stats_marqueur.py

On passe le fichier final contenant les marqueurs d'argumentation dans le script statistiques pour obtenir une liste par ordre décroissant des différents marqueurs d'argumentation utilisés dans le corpus.

## Remarques générales

Il aurait peut être été possible d'entrer directement le fichier XML produit par Lucie et Clémence dans Unitex Gramlab : cependant, quand je le faisais, seul le texte était gardé, et le fichier de sortie, à la suite de l'application des automates, était tronqué des informations concernant les contributions. J'ai donc pensé qu'il était plus simple de faire cette étape avant de changer le fichier en XML.