#!/usr/bin/env python3
# -*- coding: utf-8 -*-

""" stats_marqueur.py : statistiques sur les marqueurs

    Se lance sur les fichiers de sortie du script RemetChevrons.py : fichier_final_marqueurs.xml
        python3 stats_marqueur.py nom_fichier_entree
    

    Sortie :
        stats_marqueur.txt : liste de marqueurs avec leur nombre d'occurrences classée par ordre décroissant
"""

import sys
import xml.etree.ElementTree as ET

def entiteFreq(liste):
    dico = dict()
    for entite in liste:
        if dico.get(entite) == None:
            dico[entite] = 1
        else:
            dico[entite] += 1
    return dico

def printDecroissant(dico, fichier):
    with open(fichier, 'w', encoding="utf-8") as f_out:
        for entite in dico:
            f_out.write("{}\t{}\n".format(entite[0], entite[1]))
    print("Le fichier {} a été créé.".format(fichier))

tree = ET.parse(sys.argv[1])

root = tree.getroot()

i = 0

marqueur = []

for child in root.iter('marqueur'):
    child_min = child.text.lower()
    marqueur.append(child_min)
    i += 1

dicoMarqueur = entiteFreq(marqueur)

dico_marqueur = sorted(dicoMarqueur.items(), key=lambda t:t[1], reverse=True)

printDecroissant(dico_marqueur, 'stats_marqueur.txt')