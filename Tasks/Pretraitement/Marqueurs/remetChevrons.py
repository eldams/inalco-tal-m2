# Script qui remplace les entitées xml pour les chevrons, par des balises bien formées 

# à lancer après le script fromRawtoXml.py

# se lance de la manière suivante : python3 remetChevrons.py > fichier_final_marqueurs.xml


import re
from lxml import etree


with open("GDN_marqueur_struct.xml", "r", encoding="utf-8") as FileOut:
    
    for line in FileOut:
        line = re.sub(r"&lt;marqueur&gt;" , "<marqueur>", line)
        line = re.sub(r"\b&lt;/marqueur&gt;" , "</marqueur>", line)
        print(line)