#!/usr/bin/env python3
# -*- coding: utf-8 -*-

""" entites_nommees_spacy.py : ajoute un balisage des entités reconnues par spacy au fichier GDN_struct.xml

    Se lance sur le fichier de sortie du script prenormalisation.py :
        python3 entites_nommees_spacy.py

    Sortie : GDN_pretrait_entites_spacy.xml
"""

import spacy
import xml.etree.ElementTree as ET

nlp = spacy.load("fr_core_news_sm")

tree = ET.parse("GDN_struct_prenorm.xml")
root = tree.getroot()

# Parcours des contributions
for child in root:
    liste = ""
    doc = nlp(child.text)
    for token in doc:
        # Recherche d'entités LOC, PER ou ORG
        if token.ent_type_ != "":
            if token.ent_type_ != "MISC":
                liste= liste + ("<{}>{}</{}>".format(token.ent_type_, token.text, token.ent_type_)) + " "
            else:
                liste = liste + (token.text) + " "
        else:
            liste = liste + (token.text) + " "
    child.text = str(liste)

tree.write("GDN_pretrait_entites_spacy.xml", encoding="utf-8", xml_declaration=True, method="xml")