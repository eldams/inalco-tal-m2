#!/usr/bin/env python3
# -*- coding: utf-8 -*-

""" stats.py : statistiques sur les entités LOC, PER et ORG d'un fichier

    Se lance sur les fichiers de sortie du script normalisation.py (sortie spacy ou flair)
        python3 stats.py nom_fichier_entree
    
    Entrée pour flair :
        GDN_entites_flair.xml

    Entrée pour spacy :
        GDN_entites_spacy.xml

    Sortie :
        stats.txt : nombre d'étiquettes par catégorie d'entité nommée et leur distribution (%)
        stat_LOC.txt : liste ordonnée des entités LOC avec leur fréquence
        stat_ORG.txt : liste ordonnée des entités ORG avec leur fréquence
        stat_PER.txt : liste ordonnée des entités PER avec leur fréquence
        stat_total.txt : liste ordonnée de toutes les entités
"""

import sys
import xml.etree.ElementTree as ET

def entiteFreq(liste):
    dico = dict()
    for entite in liste:
        if dico.get(entite) == None:
            dico[entite] = 1
        else:
            dico[entite] += 1
    return dico

def printDecroissant(dico, fichier):
    with open(fichier, 'w', encoding="utf-8") as f_out:
        for entite in dico:
            f_out.write("{}\t{}\n".format(entite[0], entite[1]))
    print("Le fichier {} a été créé.".format(fichier))

tree = ET.parse(sys.argv[1])

root = tree.getroot()

i = 0

org = []
per = []
loc = []

for child in root.iter('PER'):
    per.append(child.text)
    i += 1

for child in root.iter('LOC'):
    loc.append(child.text)
    i += 1
  
for child in root.iter('ORG'):
    org.append(child.text)
    i += 1

dicoOrg = entiteFreq(org)
dicoPer = entiteFreq(per)
dicoLoc = entiteFreq(loc)

dicoEntite = dict()

dicoEntite.update(dicoOrg)
dicoEntite.update(dicoPer)
dicoEntite.update(dicoLoc)

dico_org = sorted(dicoOrg.items(), key=lambda t:t[1], reverse=True)

dico_per = sorted(dicoPer.items(), key=lambda t:t[1], reverse=True)

dico_loc = sorted(dicoLoc.items(), key=lambda t:t[1], reverse=True)

dico_total = sorted(dicoEntite.items(), key=lambda t:t[1], reverse=True)

printDecroissant(dico_org, 'stat_ORG.txt')
printDecroissant(dico_per, 'stat_PER.txt')
printDecroissant(dico_loc, 'stat_LOC.txt')
printDecroissant(dico_total, 'stat_total.txt')


with open('stats.txt', 'w', encoding="utf-8") as f_out:
    f_out.write('Répartition du nombre d\'entités nommées : \n\tOrganisation : {} \n\tPersonne : {} \n\tLieu : {} \n\tTotal : {}\n\n'.format(len(org), len(per), len(loc), i))
    f_out.write('Organisation : {} %\n'.format(len(org)/i*100))
    f_out.write('Personne : {} %\n'.format(len(per)/i*100))
    f_out.write('Lieu : {} %'.format(len(loc)/i*100))

print("Le fichier stats.txt a été créé.")