#!/usr/bin/env python3
# -*- coding: utf-8 -*-

""" prenormalisation.py : désaccentualise les majuscules avant de les minusculiser

    A lancer sur le fichier GDN_struct.xml :
        python3 prenormalisation.py GDN_struct.xml > GDN_struct_prenorm.xml
"""

import re
import sys

def sublower(matchobj):
    """ Minusculise le groupe capturé d'une expression régulière """
    return matchobj.group(1).lower()
    
with open(sys.argv[1], "r", encoding="utf-8") as FileOut:
    for line in FileOut:
        
        line = re.sub(r"&lt;marqueur&gt;" , "", line)
        line = re.sub(r"&lt;/marqueur&gt;" , "", line)
        
        # Normalisation des majuscules diacritées
        line = re.sub(r"ÔÖ", r"O", line)
        line = re.sub(r"Ç", r"C", line)
        line = re.sub(r"ÉÈÊ", r"E", line)
        line = re.sub(r"ÛÜ", r"U", line)
        line = re.sub(r"ÀÂÄ", r"A", line)
        line = re.sub(r"ÎÏ", r"I", line)
        line = re.sub(r"Œ", r"OE", line)
        line = re.sub(r"Æ", r"AE", line)
        
        # Recolle le dernier caractère d'un sigle qui n'est pas suivi par un point
        line = re.sub(r"([a-z]\s(?:[A-Z]\.)+) ([A-Z] )", r"\1\2", line)
        
        # Met entre balises ££ les suites de tokens en majuscule
        # Tient compte d'autres caractères qui pourraient se trouver à l'intérieur de la suite (0-9,°, espace, =,<,>,&) dans le texte original
        line = re.sub(
            r"((?:[A-Z',0-9°\s(?:&amp;)(?:=?&gt;)(?:&lt;)]+ ){3}(?:[A-Z',0-9°\s(?:&amp;)(?:=?&gt;)(?:&lt;)]+ *)*)(\-\(\.!\?(?:</contrib>))*", 
            r"&lt;££&gt;\1\2&lt;/££&gt;", 
            line
        )

        # Minusculise les tokens qui sont entre balises ££
        line = re.sub(r"&lt;££&gt; *(.+?) *&lt;/££&gt;", sublower, line)
        
        # Minusculise les mots en début de phrase
        line = re.sub(r"([\.!\?] [A-Z][^\.])", sublower, line)
        
        print(line)