# Prétraitement - Entités
* Afala, Lili *

## Commandes pour lancer les différents scripts

| Nom du script			     | Commande à effectuer                         |
| :------------------------: | :------------------------------------------: |
| *prenormalisation.py*      | `python prenormalisation.py entree > sortie` |
| *entites_nommees_spacy.py* | `python entites_nommees_spacy.py`            |
| *entites_nommees_flair.py* | `python entites_nommees_flair.py`            |
| *normalisation.py*         | `python normalisation.py entree > sortie`    |
| *stats.py*                 | `python stats.py entree`                     |

## Entrée
Document XML produit par la Restructuration : GDN_struct.xml

## Sortie
Documents structurés au format XML avec balises entités

## Traitements opérés sur le corpus

* minusculisation des contributions en majuscules
* ajout de balises entités
* décodage des entités qui représentent les chevrons -> avec le script normalisation.py
* statistiques

### Minusculisation des contributions en majuscules

Afin d'éviter les erreurs dans la reconnaissance des entités nommées avec flair, nous avons voulu minusculiser les suites de plus de trois tokens en majuscules. Pour cela, nous avons au préalable désaccentué les majuscules.

### Ajout d'entités

Nous avons voulu ajouter des balises pour les entités. Les balises que nous avons décidé de garder sont PER (personne), ORG (organisaiton) et LOC (lieux).

* mise en forme des sorties flair
Ce que l’on obtient : 
B (beginning) : début, I (inside) : intérieur, E (ending) : fin
> France <S-LOC> : un seul token
> Monsieur <B-PER> Macron <E-PER> :  deux tokens
> Hauts <B-LOC> de <I-LOC>  France <E-LOC> : trois tokens
Ce que l’on veut obtenir :
> <LOC>France</LOC> : un seul token
> <PER>Monsieur Macron</PER> :  deux tokens
> <LOC>Hauts de France</LOC> : trois tokens

### Normalisation

* exécuter le script normalisation.py sur les fichiers de sortie des scripts de spacy et de flair

#### SpaCy
Comme spacy tokenise les données en entrée, nous les avons réalignés avec un espace entre chaque token, ce qui laisse des espaces avant les signes de ponctuation. Pour y remédier, nous les avons supprimé avec le script normalisation.py.

Les chevrons des balises entités contenus dans le fichier de sortie précédent avaient été changés en entités XML, on applique le script pour remplacer les entités pour les chevrons par le symbole correspondant.

#### FlairNLP
Flair ajoute un espace des deux côtés de la ponctuation sauf pour le point, où il n'y a pas d'espace après. Certaines entités de flair, composés de plusieurs tokens, n'avient pas de token de début ou de token de fin, ce qui a donné des entités pour lesquelles il manquait soit la balise ouvrante, soit la balise fermante. Nous avons donc utilisé le script de normalisation pour supprimer les espaces redondants (pour la ponctuation, les sigles) ainsi que les balises entités mal formées.

### Statistiques
- 5 fichiers:
    - nombre d'étiquettes par catégorie d'entité nommée et leur distribution (%) : stats.txt
    - liste ordonnée des entités LOC avec leur fréquence : stat_LOC.txt
    - liste ordonnée des entités ORG avec leur fréquence : stat_ORG.txt
    - liste ordonnée des entités PER avec leur fréquence : stat_PER.txt
    - liste ordonnée de toutes les entités : stat_total.txt

D'après les résultats des statistiques, nous avons observé une meilleure reconnaissance des entités avec flair.

### Remarques générales

Le temps d'exécution du script est très chronophage en soi car cela peut (avec flair) prendre plusieurs jours si celui-ci est lancé sur un ordinateur ne possédant pas un GPU assez conséquent. Nous avons donc choisi d'utiliser google colaboratory qui a permis de réduire la durée d'exécution à environ 3 et 5 heures.

Comme les résultats de flair étaient relativement meilleurs que ceux de spacy, nous nous sommes plutôt concentrés flair.

Nous n’avons pas pu conserver les contributions en majuscules car flair ne s’y adapte pas très bien, ce qui conduisait la majeure partie du temps, à de mauvais résultats.
Nous avons donc décidé de minusculiser la première lettre de chaque phrase ainsi que les contributions qui contenaient plus de trois tokens tous en majuscule. Les entités nommées commencent en grande partie par une majuscule.

La fusion des fichiers XML marqueurs et entités nommées s’est révélée infructueuse. Nous n’avons actuellement pas trouvé de moyen efficace pour fusionner les deux fichiers. Cela reste une piste à creuser.
