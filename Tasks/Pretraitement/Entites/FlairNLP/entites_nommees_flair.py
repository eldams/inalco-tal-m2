#!/usr/bin/env python3
# -*- coding: utf-8 -*-

""" entites_nommees_flair.py : ajoute un balisage des entités reconnues par flair au fichier GDN_struct.xml

    Se lance sur le fichier de sortie du script prenormalisation.py :
        python3 entites_nommees_flair.py

    Sortie : GDN_pretrait_entites_flair.xml
"""

import re
import sys
import xml.etree.ElementTree as ET
from flair.data import Sentence
from flair.models import SequenceTagger
from segtok.segmenter import split_single

import time
from datetime import timedelta

start = time.time()
with open("GDN_flair_final.log","w",encoding="utf-8") as log:
    tree = ET.parse('GDN_struct_prenorm.xml')

    root = tree.getroot()

    tagger = SequenceTagger.load('fr-ner')

    i = 0 # compteur pour les contributions
    
    for child in root:
        sentences = [Sentence(sent, use_tokenizer=True) for sent in split_single(child.text)]
        contrib2 = ""
        for sentence in tagger.predict(sentences):
            contrib = sentence.to_tagged_string()
            contrib2 = contrib2 + contrib
        i += 1

        # Imprime sur la sortie standard le numéro de contribution analysé pour indiquer la progression de l'exécution du script 
        print(i)
        child.text = str(contrib2)
        # suppression de la catégorie d'entité MISC (divers)
        child.text = re.sub(r"\<[SBIE]-MISC\>", r"",child.text)
        
        # ajoute un espace dans certaines contributions où flair colle un point avec le mot qui suit
        child.text = re.sub(r"(\.)([A-Za-z][^\.])", r"\1 \2",child.text)
        
        # modification de la mise en forme de l'étiquetage pour les entités en balises
        child.text = re.sub(r"(\S+) \<S-([A-Z]+?)\>", r"<\2>\1</\2>",child.text)
        child.text = re.sub(r"(\S+) \<B-([A-Z]+?)\>", r"<\2>\1",child.text)
        child.text = re.sub(r"(\S+) \<E-([A-Z]+?)\>", r"\1</\2>",child.text)
        child.text = re.sub(r"\<I-([A-Z]+?)\>", r"",child.text)

    tree.write('GDN_pretrait_entites_flair.xml', encoding="utf-8", xml_declaration=True, method="xml")

    duree = time.time() - start

    log.write("Durée de traitement du corpus : {} secondes.\n".format(timedelta(seconds=round(duree))))