#!/usr/bin/env python3
# -*- coding: utf-8 -*-

""" normalisation.py : remplace les entités HTML des chevrons par leur symbole correspondant et supprime les espaces redondants autour des ponctuations causés par flair.

    Se lance sur le fichier de sortie du script flair ou spacy :
        python3 normalisation.py nom_fichier_entree > nom_fichier_sortie

    Sortie pour flair
        nom_fichier_entree : GDN_pretrait_entites_flair.xml
        nom_fichier_sortie : GDN_entites_flair.xml

    Sortie pour spacy
        nom_fichier_entree : GDN_pretrait_entites_spacy.xml
        nom_fichier_sortie : GDN_entites_spacy.xml
"""

import re
import sys

def balises(line, ent):
    """ Décodage des entités &lt; et &gt; """
    line = re.sub(r"&lt;{}&gt;".format(ent) , "<{}>".format(ent), line)
    line = re.sub(r"&lt;/{}&gt;".format(ent) , "</{}>".format(ent), line)
    return line

def subupper(matchobj):
    """ Majusculise le groupe capturé d'une expression régulière """
    return matchobj.group(1).upper()
    
with open(sys.argv[1], "r", encoding="utf-8") as FileOut:
    for line in FileOut:
        line = line.rstrip()
        
        # Traitement pour les chevrons des balises
        line = balises(line, "LOC")
        line = balises(line, "PER")
        line = balises(line, "ORG")
        
        # Suppression des espaces avant ou après une ponctuation
        line = re.sub(r" +\." , ".", line)
        line = re.sub(r" +," , ",", line)
        line = re.sub(r" +;" , ";", line)
        line = re.sub(r" +…" , "…", line)
        line = re.sub(r"(?<=[^(version='1.0)])' ", "'", line)
        
        # Suppression des doubles espaces causés par le passage dans le script flair (notamment par la suppression des étiquettes des tokens positionnés au milieu (I) d'entités composées de plusieurs tokens de flair)
        line = re.sub(r"  ", " ", line)
        
        # Recollage des sigles décollés lors du passage dans le script flair
        line = re.sub(r"([^A-Z][A-Z]\.)\s(?=[A-Z]\.)", r"\1", line)
        line = re.sub(r"(\.[A-Z]\.)\s+(?=[A-Z]\.)", r"\1", line)
        
        # Si la derniere lettre ne possède pas de point
        line = re.sub(r"([a-z]\s(?:[A-Z]\.)+) ([A-Z] )", r"\1\2", line)
        
        # Idem pour les eventuels sigles écrits en minuscule
        line = re.sub(r"([^a-z][a-z]\.)\s+(?=[a-z]\.)", r"\1", line)
        line = re.sub(r"(\.[a-z]\.)\s+(?=[a-z]\.)", r"\1", line)
        
        # Suppression des balises mal formées de flair
        line = re.sub(r"(<PER>Michel Revol</PER> du Point)</ORG>", r"\1", line)
        line = re.sub(r"(<LOC>Sargasses</LOC> aux )<LOC>(Antilles !)", r"\1\2", line)
        line = re.sub(r"(<ORG>SNCF</ORG> Parie Limoges)</LOC>", r"\1", line)
        
        # Remise des majuscules en début de phrase
        line = re.sub(r"([\.!\?] [a-z])(?=[^\.])", subupper, line)
        
        print(line)