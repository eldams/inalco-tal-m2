## Prétraitement
Maeva, Lili, Afala

Lili, Afala : entités nommées
Maeva : marqueurs

## Entrée : 
Pour les marqueurs:
-> fichier TXT (aller voir le README.MD du dossier Marqueurs)
Pour les entités nommées:
-> fichier XML produit par le groupe Restructuration
## Sortie : 
1 fichier XML avec des balises marqueurs ajoutées
2 fichiers XML avec des balises entités nommées
5 fichiers stats pour chaque fichier XML entités nommées
1 fichier stats pour le fichier XML marqueurs
## Traitement : 
FlairNLP et SpaCy pour les entités nommées, Unitex pour les marqueurs