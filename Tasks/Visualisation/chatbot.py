# Projet Outils de tratement de corpus
# Groupe : Visualisation
# Partie fonction chatbot
# python3
import difflib

# retourne une liste d'arguments en string
def get_list_args():
    
    with open("args_extraits_brut_final.txt", "r", encoding="utf8") as texte:
        read_data=texte.readlines()
        
        # supprimer les interlignes
        data = [ x for x in read_data if x != '\n' ] 

        #supprimer les '\n' à la fin des phrases
        data = [ x.replace('\n','') for x in data if '\n' in x ] 

        return data

# comparer 2 strings et calcul le taux de similarité
def string_similar(s1, s2):
    return difflib.SequenceMatcher(None, s1, s2).quick_ratio()

# générer l'argument pertinent avec 2 phrases de plus haute similarité
def get_arg_similaire(input_string,data):
    dict_simi = {}

    for a in data:
        reponse = string_similar(input_string, a)
        dict_simi[a] = reponse

    dict_perti = {k: v for k, v in sorted(dict_simi.items(), key=lambda item: item[1],reverse=True)}
    first10pairs = {k: dict_perti[k] for k in list(dict_perti)[:10]}
    first3pairs = {k: dict_perti[k] for k in list(dict_perti)[:3]}
    first_arg = list(dict_perti.keys())[0]
    second_arg = list(dict_perti.keys())[1]
    reponse = first_arg+' '+second_arg
    reponse = reponse.replace('"','')
    return reponse


# --------- main --------------

# récupère la liste des arguments
data = get_list_args()

# Boucle pour chatbot pour tester et jouer avec, 
while True:
    input_string = input('Tapez votre phrase : \n\t')
    reponse = get_arg_similaire(input_string,data)
    print('Réponse chatbot : '+reponse)
