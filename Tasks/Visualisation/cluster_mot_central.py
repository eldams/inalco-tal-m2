#utf_8

from sklearn.feature_extraction.text import TfidfVectorizer
from pprint import pprint
import treetaggerwrapper
import re

def creationList(read_data):
    """
    Sépare le contenu du fichier par thématiques.
    
    Arguemnts:
        la liste des lignes du fichier
    Return:
        liste_des_strings  - la liste les arguments par thématique
        noms_des_categories - la liste des noms des catégories(thématiques)
    """
    temporary_string=""
    liste_des_strings=[]
    liste_cats=[]
    for line in read_data:
        if line=="\n":
            liste_des_strings.append("\n".join(temporary_string.split("\n")[1:]))
            liste_cats.append(temporary_string.split("\n")[0])
            temporary_string=""
            
        else:
            temporary_string=temporary_string+line
    #print(liste_cats)
    return liste_des_strings, liste_cats




def centralWordCalculator(sent_max):
    """
    Normalise le texte et renvoie les lemmes
    
    Arguments:
        Texte
        
    Renvoie:
        liste des lemmes pertinents
    """

    tagger=treetaggerwrapper.TreeTagger(TAGLANG='fr')

    list_tagged = []

    for tex in sent_max:
        tags=tagger.tag_text(tex)
        #print('----')
        tags2=treetaggerwrapper.make_tags(tags)
        #print(tags2)
        empty=[]
        for tag in tags2:
            tagg=tag
            empty.append(tagg)
            #print(empty)
    
        grammar=[]
        for element in empty:
            compt=0
            for i in element:
                if compt==1 or compt==2:
                    grammar.append(i)
                    grammar.append("\t")
                compt+=1
            grammar.append("\n")
        
        grammar = grammar[:-1]
        res="".join(grammar)
    
    
        ress=res.split("\n")
        
        lemmes=[]
        for rrr in ress:
            if len(rrr)==0 or "@" in rrr:
                del ress[ress.index(rrr)] 
        for rr in ress:
        
            match_tag = re.search(r"(.*)\t(.*)\t", rr)

            if "VER" in match_tag.group(1) or "NOM" in match_tag.group(1) or "ABR" in match_tag.group(1) or "ADJ" in match_tag.group(1) :
                lemmes.append(match_tag.group(2).lower())
            elif "VV" in match_tag.group(1) or "NN" in match_tag.group(1) or "NP" in match_tag.group(1) or "JJ" in match_tag.group(1) or "VH" in match_tag.group(1) or "VB" in match_tag.group(1) or "MD" in match_tag.group(1) :
                lemmes.append(match_tag.group(2).lower())

        list_tagged.append(lemmes)

    return list_tagged



def tfidfCalculator(text):
    counter=0
    central_words=[]
    vectorizer = TfidfVectorizer()
    # tokenize and build vocab
    vectorizer.fit_transform(text)
    idf=vectorizer.idf_
    dictionary = dict(zip(idf, vectorizer.get_feature_names()))
    #dictionary=sorted(dictionary.key(), reverse=True)
    
    for key in sorted(dictionary.keys(), reverse=True):
        if dictionary[key] not in ['ëtre','êtres','œuf']: # pour enlever le mot 'être'
            if counter<5:
                central_words.append(dictionary[key])
                counter+=1
    return central_words 



def prosessus():
    
    with open ("args_extraits_brut_final.txt", 'r') as textee:
        read_data=textee.readlines()
    
    # on récupère la liste des arguments(par cétégorie) et les catégories respectives
        arguments, noms_des_categories=creationList(read_data)
        dico_clusters=dict()
    
        with open("repartition_{}.txt".format("clusters"), 'w') as rees:
            rees.write("")
            
        with open('mots_central_par_catégorie.txt','w') as fout:
            for n in noms_des_categories:
                val_max=0
                lemmas=[]
                with open('{}.txt'.format(n), 'r') as fich:
                    lines=fich.readlines()
                
                    for line in lines:
                        line=line.split(":")
                        if int(line[0]) not in dico_clusters.keys():
                            dico_clusters[int(line[0])]=[]
                        else:
                            dico_clusters[int(line[0])].append(line[1])
                        
                    with open("repartition_{}.txt".format("clusters"), 'a') as res:
                        res.write("{}\n".format(n))
                    for key in dico_clusters.keys():
                        
                    #print(key)
                        #print("{} :\t{}".format(key,len(dico_clusters[key])))
                        
                        if len(dico_clusters[key])>val_max:
                            val_max=len(dico_clusters[key])
                            sent_max=dico_clusters[key]
                            key_max=key
                    
                        with open("repartition_{}.txt".format("clusters"), 'a') as   ress:
                            ress.write("{} :\t{}\n".format(key,len(dico_clusters[key])))
                        
                    with open("repartition_{}.txt".format("clusters"), 'a') as ress:
                        ress.write("\n")
                        print("writing repartition file and Calculating central words...")
                    
                    print(n)
                    fout.write(n)
                    fout.write('\n')
                    print(val_max)
                    print(key_max)
                    
                    sent_max = [w.replace('’', "'") for w in sent_max]

                    lemma=centralWordCalculator(sent_max)

                    for l in lemma:
                        sentence = " ".join(m for m in l)
                        lemmas.append(sentence)
                        
                    central_words=tfidfCalculator(lemmas)
                    
                    print(central_words)
                    fout.write(str(central_words))
                    fout.write('\n\n')
                
                    
                    
if __name__ == "__main__":
    prosessus()
