# Application : visualisation
- Xin, Emilie (création de l'application de visualisation et présentation graphique des données)
- Siyu, Alexandr (traitement de corpus et calcul de similarité，cosinus, et clustering)
## Entrées : 
- Résultats de l'étape Détection (classification et annotation)
## Sorties : 
- Une application pour présenter des répartitions graphiques des arguments par similarité, tableaux de classement
- Zone de texte à jouer par les utilisateurs qui prend en input une phrase d'argument puis sortir une phrase d'argument similaire

## Traitement : 
PARSER LE XML (BeautifulSoup, lxml):

1. Trouver les balises "argument à l'intérieur de chaque balise "contrib".
 arguments = soup.find_all('argument')


2. Récupérer le texte. 

for ar in arguments:
        argss=ar.get_text()
      
      
        

TRAITEMENT DES DONNEES (sklearn):       

Tokenisation:
FT = FlaubertTokenizer.from_pretrained('argss')


Calcul de similarité cosinus:
1. Vectorisation de chaque argument avec le calcul de tfidf (sklearn.TfidfVectorizer). 

 - create the transform
vectorizer = TfidfVectorizer()
 - tokenize and build vocab
vectorizer.fit(text)

vectorizer.vocabulary_
vectorizer.idf_

- transform the vector
vector = vectorizer.transform([text[0]])


2. Calcul de la distance cosinus (sklearn.cosine_similarity)

array=cosine_similarity(vector1, vector2)

3. Clustering : 
- clusterisation des arguments à l'intérieur de chaque catégorie(K-Means)
- recherche des mots centraux par thématique
- sortir un fichier contenant le cluster de chaque argument par thématique


CHATBOT:

1. fonction "argument" qui récupère l'input utilisateur et retourne l'argument le plus proche. 

MAIN:
input_arg = input("Your argument : ")
while input_arg != "_stop":
    print("Réponse :", argument(input_arg))
    input_arg = input("Your argument : ")




APPLICATION WEB POUR VISUALISER

- outils utilisés:
1. Streamlit (création d'une application de visualisation interactive).
2. Plotly (génération des graphes interactifs).

- Se compose de trois parties:
1. Introduction (Présentation des données et du fonctionnement du chatbot).

2. Répartition (Les graphes qui présentent les différentes données + un tableau de classement qui présente les mots centraux par catégorie) + 

3. A vous de jouer (Un petit chatbot qui retourne l'argument similaire). 


