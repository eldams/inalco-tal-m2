#! /usr/bin/python3
# -*- coding: utf-8 -*-

# ----------------------------------------------------------------------------------------
# streamlit run visualisation.py
# 
# Projet Outils de tratement de corpus
# Groupe : Visualisation
#----------------------------------------------------------------------------------------
import streamlit as st
import re
import pandas as pd
import plotly.express as px
import plotly.graph_objects as go
from PIL import Image
import lxml
from lxml import etree
from collections import Counter
import difflib

# ---------fonctions chatbot---------------
def get_list_args():
    """
    retourne une liste d'arguments en string
    """
    with open("args_extraits_brut_final.txt", "r", encoding="utf8") as texte:
        read_data=texte.readlines()
        
        # supprimer les interlignes
        data = [ x for x in read_data if x != '\n' ] 

        #supprimer les '\n' à la fin des phrases
        data = [ x.replace('\n','') for x in data if '\n' in x ] 

        return data

def string_similar(s1, s2):
    """
    comparer 2 strings et calcul le taux de similarité
    """
    return difflib.SequenceMatcher(None, s1, s2).quick_ratio()

def get_arg_similaire(input_string,data):
    """
    générer l'argument pertinent avec 2 phrases de plus haute similarité
    """
    dict_simi = {}

    for a in data:
        reponse = string_similar(input_string, a)
        dict_simi[a] = reponse

    dict_perti = {k: v for k, v in sorted(dict_simi.items(), key=lambda item: item[1],reverse=True)}
    first10pairs = {k: dict_perti[k] for k in list(dict_perti)[:10]}
    first3pairs = {k: dict_perti[k] for k in list(dict_perti)[:3]}
    first_arg = list(dict_perti.keys())[0]
    second_arg = list(dict_perti.keys())[1]
    reponse = first_arg+' '+second_arg
    reponse = reponse.replace('"','')
    return reponse

# récupère la liste des arguments
data = get_list_args()

# ---------génération des graphes et tableau---------------
def graph_categorie(xmlfile) :
    """
        Crée un graph en forme de camembert pour la répartition des contributions en différentes thématiques.

        input : fichier_final_marqueurs.xml
        output : une figure
    """
    tree = etree.parse(xmlfile)
    root = tree.getroot()

    nb_cat = []
    cat = []
    cnt = Counter()

    for element in root :
        cnt[element.get('cat')] += 1

    for a, b in cnt.items():
        cat.append(a)
        nb_cat.append(b)

    fig = go.Figure(data=[go.Pie(labels = cat, values=nb_cat)])
    fig.update_layout(
        autosize=False,
        width=800,
        height=500,)
    return fig

def graph_freq_args(fichier) :
    """
        Crée un graph en forme de camembert pour la répartition des fréquences d'argument par thématique.
        
        input : repartition_themes_freq.txt
        output : une figure
    """
    cat = []
    freq_cat = []

    with open(fichier, "r", encoding="utf8") as f :
        doc = f.read()
        lines = doc.splitlines()
        for line in lines[2:] :
            element = line.split('\t')
            cat.append(element[0])
            freq_cat.append(element[1])

    fig = go.Figure(data=[go.Pie(labels = cat, values=freq_cat)])
    fig.update_layout(
        autosize=False,
        width=800,
        height=500,)

    return fig

def graph_repartition_cluesters(fichier) :
    """
        Crée un graph en bâton pour la répartition du nombre d'argument par cluster.
        
        input : repartition_clusters.txt
        output : une figure
    """
    cat = []
    zero = []
    un = []
    deux = []
    with open(fichier, "r") as f:
        lines = f.readlines()
        for line in lines :
            if line != "\n" :
                line = line.strip()
                element = re.split(' :\t|\n', line)
                
                if len(element) == 1 :
                    cat.append(element[0])
                if element[0] == "0":
                    zero.append(element[1])
                if element[0] == "1":
                    un.append(element[1])
                if element[0] == "2":
                    deux.append(element[1])

        fig = go.Figure(data=[
            go.Bar(name='Cluster : 0', x=cat, y=zero), 
            go.Bar(name='Cluster : 1', x=cat, y=un), 
            go.Bar(name='Cluster : 2', x=cat, y=deux)
        ],
        layout=go.Layout(
            xaxis_title="Cluster pour les différentes catégories",
            yaxis_title="Nombre d'argument",
            barmode='group'
        ))
        return fig

def tableau_mots_centraux(fichier) :
    """
        Création d'une liste de listes avec les différents mots centraux pour le tableau.
    """
    liste_colonne = []
    ponctuation = "[]',"
    with open(fichier, "r", encoding="utf8") as f:
        lines = f.readlines()
        for line in lines :
            if '[' in line :
                line = line.strip()
                table = str.maketrans("", "", ponctuation)
                line_sans_ponct = line.translate(table)
                colonne = line_sans_ponct.split(' ')
                liste_colonne.append(colonne)
    return liste_colonne

# ---------gestion de l'historique---------------
def sauvgardeHistorique(entree, sortie):
    with open("./historique.log", "a", encoding="utf8") as fi:
        fi.write(f"{entree}\t{sortie}\n")

def afficheHistorique():
    entrees = []
    reponses = []
    with open("./historique.log", "r", encoding="utf8") as fi:
        for line in fi:
            entree, reponse = line.strip().split('\t')
            entrees.append(entree)
            reponses.append(reponse)

    st.table(pd.DataFrame({'Votre phrase': entrees, 'Notre réponse':reponses}))

# ---------visualisation---------------
st.sidebar.markdown("## Menu")
menu = st.sidebar.radio(
    "Go to",
    ('👨‍🏫 Introduction', '📊 Répartition', '🤖 Jeux'))
st.sidebar.markdown("## À propos")
st.sidebar.info("Cette application est maintenu par M. Nouvel et la promotion 2019 TAL IM Inalco. Vous pouvez consulter le projet en détail sur le gitlab [inalco-tal-m2](https://gitlab.com/el-dams/inalco-tal-m2).")
st.title('Projet Outils de traitement de corpus')
st.header('[INALCO TAL](http://www.inalco.fr/formations/formations-diplomes/accueil-formations-diplomes/masters/master-tal) - 2020')


if menu == '👨‍🏫 Introduction':
    st.markdown("Enseignant : [Damien NOUVEL](http://damien.nouvels.net/)")
    st.markdown("## **Introduction du projet** ")
    st.markdown("Ce projet est fait par les étudiants de Master 2 Ingénierie Multilingue à l'INALCO.")
    st.markdown("L'objectif global est de créer un système qui retourne l'argument similaire à celui saisi par l'utilisateur.")
    st.markdown("[Le corpus utilisé](http://damien.nouvels.net/bazar/GDNvsVD-data.zip) dans ce projet est un corpus qui se compose des contributions lors du [Grand Débat National](https://granddebat.fr) et se compose de **342 097** contributions qui sont séparéees par thématiques :")
    st.markdown("<li>la fiscalite et les depenses publiques</li>", unsafe_allow_html=True)
    st.markdown("<li>la transition ecologique</li>", unsafe_allow_html=True)
    st.markdown("<li>democratie et citoyennete</li>", unsafe_allow_html=True)
    st.markdown("<li>organisation de letat et des services publics</li>", unsafe_allow_html=True)
    st.markdown("")
    st.markdown("Pour atteindre l'objectif le travail est partagé entre 4 groupes:")
    st.markdown("**👩‍💻👩‍💻 Groupe Restructuration**")
    st.markdown("Le travail de ce groupe consiste à :")
    st.markdown("<li>nettoyer le corpus, par exemple la normalisation de la syntaxe de la ponctuation et des majuscules</li>", unsafe_allow_html=True)
    st.markdown("<li>le structurer en XML permettant de relier chaque contribution à son thème/sa catégorie</li>", unsafe_allow_html=True)
    st.markdown("<li>générer des ressources lexicales et des statistiques globales</li>", unsafe_allow_html=True)
    st.markdown("")

    st.markdown("**👩‍💻👩‍💻👩‍💻 Groupe Prétraitement**")
    st.markdown("La méthodologie utilisée de ce groupe contient deux étapes :")
    st.markdown("<li>la création des automates pour identifier les marqueurs et l'ajout de balises marqueurs(outil: Unitex Gramlab)</li>", unsafe_allow_html=True)
    st.markdown("<li>le prétraitement des entités nommées et l'ajout de balises entités comme PER (personne), ORG (organisaiton) et LOC (lieux) (outils: SpaCy et FlairNLP)</li>", unsafe_allow_html=True)
    st.markdown("")

    st.markdown("**👩‍💻👨‍💻👨‍💻 Groupe Detection**")
    st.markdown("Pour détecter les arguments ainsi que les propositions à partir de caractériques linguistiques, ce group utilise l'outil Stanza et construit deux méthodologies pour extraire ces arguments et propositions:")
    st.markdown("<li>un système symbolique à partir de règles syntaxiques</li>", unsafe_allow_html=True)
    st.markdown("<li>un autre système d'apprentissage à partir de CRF sont définis et appliqués</li>", unsafe_allow_html=True)
    st.markdown("")

    st.markdown("**👩‍💻👩‍💻👩‍💻👨‍💻 Groupe Visualisation**")
    st.markdown("Le travail du groupe consiste à ressembler les résultats des groupes précédents afin de mettre en place un système qui :")
    st.markdown("<li>clusterise les arguments définis (Kmeans)</li>", unsafe_allow_html=True)
    st.markdown("<li>calcule la distance entre les arguments et retourne le plus proche</li>", unsafe_allow_html=True)
    st.markdown("<li>fait des statistiques sur les catégories et les arguments du corpus (outil: Plotly)</li>", unsafe_allow_html=True)
    st.markdown("<li>crée une représentation globale du projet (outil: streamlit)</li>", unsafe_allow_html=True)
elif menu == '📊 Répartition':
    st.markdown("##  **Répartitions graphiques**")
    st.markdown("###  Répartition des contributions en différentes thématiques")
    fig1 = graph_categorie("../Pretraitement/Marqueurs/fichier_final_marqueurs.xml")
    st.markdown("La thématique qui préocupe le plus les politiciens est liée au dépenses publiques. En général, deux sujets qui s'avèrent les plus importants pour le plublique français sont les dépenses publiques et l'écologie.")
    st.write(fig1)
    
    st.markdown("### Répartition des fréquences d'argument par thématique")
    st.markdown("Les arguments suivent la même tendance et conséquemment,  les plus nombreux sont les arguments qui portent sur les dépenses publiques et l'écologie.")
    fig2 = graph_freq_args("./repartition_themes_freq.txt")
    st.write(fig2)
    
    st.markdown("### Répartition du nombre d'argument par cluster")
    st.markdown("Le diagramme ci-dessous montre le cluster d'arguments central pour chaque catégorie. L'importance des clusters est calculée à travers la répartition des arguments entre eux et est en directe corrélation avec le volume argumentatif du cluster.")
    fig3 = graph_repartition_cluesters("./repartition_clusters.txt")
    st.write(fig3)
    
    st.markdown("### Répartition de clusters")
    st.markdown("La méthode de partionnement des données choisie est k-moyennes basé sur l'algorythme de  Lloyd-Max. Le nombre des clusters est défini par le 'Elbow Method'. Le nombre de clusters est : 3 par catégorie pour uniformiser les données et rendre la visualisation plus claire.")
    image1 = Image.open('./image_repartition_clusters/DEMOCRATIE_ET_CITOYENNETE.png')
    st.image(image1, caption='Répartition des clusters pour DEMOCRATIE_ET_CITOYENNETE', use_column_width=True)

    image = Image.open('./image_repartition_clusters/LA_FISCALITE_ET_LES_DEPENSES_PUBLIQUES.png')
    st.image(image, caption='Répartition des clusters pour LA_FISCALITE_ET_LES_DEPENSES_PUBLIQUES', use_column_width=True)

    image = Image.open('./image_repartition_clusters/LA_TRANSITION_ECOLOGIQUE.png')
    st.image(image, caption='Répartition des clusters pour LA_TRANSITION_ECOLOGIQUE', use_column_width=True)
    
    image = Image.open('./image_repartition_clusters/ORGANISATION_DE_LETAT_ET_DES_SERVICES_PUBLICS.png')
    st.image(image, caption='Répartition des clusters pour ORGANISATION_DE_LETAT_ET_DES_SERVICES_PUBLICS', use_column_width=True)
    st.markdown("")

    st.markdown("## **Tableau de classement**")
    st.markdown("Voici les mots centraux par catégorie :")
    listeDelistes = tableau_mots_centraux("./mots_central_par_catégorie.txt")
    st.table(pd.DataFrame({'Democratie et citoyennete': listeDelistes[0], 'La fiscalite et les depenses publiques':listeDelistes[1], 'La transition ecologique':listeDelistes[2], 'Organisation de letat et des services publics':listeDelistes[3]}))
    
elif menu == '🤖 Jeux':
    st.markdown("## **À vous de jouer** ")
    # récupérer le mot de recherhe saisi par l'utilisateur
    st.markdown("Saisissez votre phrase d'argument pour voir les arguments similaires du Grand Débat Nationnal, si vous vous êtes d'accord ! ")
    user_input = st.text_area("Votre phrase :")

    if st.button('Envoyer'):
        reponse = get_arg_similaire(user_input,data)
        st.markdown("### Réponse :")
        st.write(reponse)
        sauvgardeHistorique(user_input, reponse)
        st.markdown("### Historique :")
        afficheHistorique()

