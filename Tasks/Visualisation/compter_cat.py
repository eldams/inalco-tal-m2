#utf_8

def creationList(read_data):
    """
    Sépare le contenu du fichier par thématiques.
    
    Arguemnts:
        la liste des lignes du fichier
    Return:
        liste_des_strings  - la liste les arguments par thématique
        noms_des_categories - la liste des noms des catégories(thématiques)
    """
    temporary_string=""
    liste_des_strings=[]
    liste_cats=[]
    for line in read_data:
        if line=="\n":
            liste_des_strings.append("\n".join(temporary_string.split("\n")[1:]))
            liste_cats.append(temporary_string.split("\n")[0])
            temporary_string=""
            
        else:
            temporary_string=temporary_string+line
    print(liste_cats)
    return liste_des_strings, liste_cats




nb_args=dict()
with open("repartition_themes_freq.txt", "w") as ress:
                ress.write("Les thématiques par ordre décroissant : fréquence des arguments\n\n")


with open("args_extraits_brut_final.txt", "r") as texte:
    read_data=texte.readlines()
    
    # création de la liste des arguments par catégorie et la liste des noms des catégories
    liste_des_strings, liste_cats=creationList(read_data)
    
    #vérifie si les deux listes sont de la même longueur
    if len(liste_des_strings)==len(liste_cats):
        
        #création du dictionniares cle : le nombre des arguments , valeur- le nom de la catégorie
        for r in range(0,len(liste_des_strings)):
            senteces=liste_des_strings[r].split("\n")
            nb_args[(len(senteces))]=liste_cats[r]
            
#print(nb_args)

#crée le fichier avec les nombres d'arguments par catégorie
for k in sorted(nb_args.keys(), reverse=True):
    with open("repartition_themes_freq.txt", "a") as res:
        #print("{}\t{}\n".format(nb_args[k],k))
        res.write("{}\t{}\n".format(nb_args[k],k))
            
            
            
    
