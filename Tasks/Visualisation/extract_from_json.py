# Projet Outils de tratement de corpus
# Groupe : Visualisation
# python3

import json

# LIRE UN FICHIER JSON
def lireJson(fichier_js):
    with open(fichier_js) as json_file:
        data = json.load(json_file)
    return data

data = lireJson("./args.json")
dict_cat = dict()

for i in data:
    #print(i)
    cat = i['cat']
    detected = i['detected']
    #print(detected)
    list_arg = list()
    for d in detected:
        arg = d['arg']
        list_arg.append(arg)
        
    if cat not in dict_cat.keys():
        dict_cat[cat]=list_arg
    else:
        for a in list_arg:
            dict_cat[cat].append(a)

#print(dict_cat)
with open("args_extraits_brut_final.txt", 'w') as fich:
    for k,v in dict_cat.items():
        print(k)
        fich.write(k)
        fich.write('\n')
        for x in v:
            fich.write(x)
            fich.write('\n')
        fich.write('\n')
