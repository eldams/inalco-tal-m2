from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import cosine_similarity
from gensim.models import Word2Vec
import matplotlib.pyplot as plt

import numpy as np
from sklearn.cluster import KMeans
from sklearn import cluster
from sklearn import metrics
from sklearn.decomposition import PCA
from scipy.cluster import hierarchy
from sklearn.cluster import AgglomerativeClustering






##################################### FONCTIONS ###########################

def creationList(read_data):
    """
    Sépare le contenu du fichier par thématiques.
    
    Arguemnts:
        la liste des lignes du fichier
    Return:
        liste_des_strings  - la liste les arguments par thématique
        noms_des_categories - la liste des noms des catégories(thématiques)
    """
    temporary_string=""
    liste_des_strings=[]
    liste_cats=[]
    for line in read_data:
        if line=="\n":
            liste_des_strings.append("\n".join(temporary_string.split("\n")[1:]))
            liste_cats.append(temporary_string.split("\n")[0])
            temporary_string=""
            
        else:
            temporary_string=temporary_string+line
    #print(liste_cats)
    return liste_des_strings, liste_cats




def vectoriser(sent, m):
    """
    Vectorise chaque phrase( additionner les vecteurs/nombre de mots dans la phrase)
    
    Arguments:
        SENT la liste des mots qui correspond à la phrase
        M : les vecteurs pour cette phrase
    Return:
        Le vecteur pour la phrase (np.array)
    """
    vec=[]
    numb_words=0
    for word in sent:
        if numb_words==0:
            vec=m[word]
        else:
            vec=np.add(vec, m[word])
        numb_words+=1
    return np.asarray(vec)/numb_words




def createGraph(X, nom):
    """
    La fonction genère un graph matplotlib pour la thématique donnée pour voir quel est le nombre optimal des clusteurs pour chaque thématique
    
    Arguments:
        X : le modèle vecteteurs des phrases
        NOM : le nom de la catégorie
    """
    words_c=[]

    for r in range(1,7):
        kmeans=KMeans(n_clusters=r, init= 'k-means++', random_state=42)
        kmeans.fit(X)
        words_c.append(kmeans.inertia_)
    plt.plot(range(1,7),words_c)
    plt.title("Recherche du nombre optimal des clusters pour "+nom)
    plt.xlabel("nombre de clusters")
    plt.ylabel("words_c")




def ecrireClustersFichier(X,sentences,nom, n_clusters):
    """
    Clusterise la catégorie et écrit dans le fichier correspondant le nombre du cluster avec les phrases qui s'y rapportent
    
    Arguments:
        X - le modèle vectors (np.array)
        sentences - la liste des phrases
        NOM - le nom de la catégorie
        n_clusters - le nombre de clusters défini en fonction de la présentation de la fonction créateGraph
    """
    with open("{}.txt".format(nom), "w") as initt:
        initt.write("")
    #n_clusters=3
    clf=KMeans(n_clusters=n_clusters, max_iter=100, init='k-means++', n_init=1)
    labels=clf.fit_predict(X)
    #print(labels)
    for ind, sent in enumerate(sentences):
        #print(str(labels[ind])+":"+str(sent))
        with open("{}.txt".format(nom), "a") as dem:
            dem.write(str(labels[ind])+":"+str(sent)+"\n")
    return labels, clf




def creatGraphClusters(X,n_clusters,labels, clf, nom):
    """
    Crée un graph matplotlib qui montre la répartition des clusters
    
    Arguments: 
        clf - le modèle pour clusterisation
        labels - les prédictions
        n_clusters - le nombre des clusters prédéfini
        X - les vecteurs
        nom - nom de la catégorie
    """
    pca=PCA(n_components=n_clusters).fit(X)
    cords=pca.transform(X)
    label_couleurs=["#2AB0E9", "#2BAF74", "#D7665E", "#CCCCCC"]
    couleurs=[label_couleurs[i] for i in labels]
    plt.scatter(cords[:,0], cords[:,1], c=couleurs)
    centers=clf.cluster_centers_
    centers_cord=pca.transform(centers)
    plt.scatter(centers_cord[:,0], centers_cord[:,1], marker='X', s=200, linewidths=2, c='#444d61')
    plt.title("Représentation des clusters pour "+nom)
    imageout = './image_repartition_clusters/'+nom+'.png'
    plt.savefig(imageout)
    
    
############################### MAIN #####################################
""" 
Notre travail du clacul de proximité entre les différents arguments se fait en étapes suivants:
    1. Vectorisation des phrases récupérée par thématique (Word2Vec)
    2. Définition du nombre des clusters (à l'aide de la représentation graphique)
    3. Creation des fichiers avec les clusters et les phrases correspondantes
    4. Représentation graphique des clusters
"""

# Ouverture du fichier et lecture ligne par ligne
with open ("args_extraits_brut_final.txt", 'r') as textee:
    read_data=textee.readlines()
    
    # on récupère la liste des arguments(par cétégorie) et les catégories respectives
    arguments, noms_des_categories=creationList(read_data)
    
    
    for argu, n in zip(arguments, noms_des_categories):
        sentences=[]
        argu=argu.split("\n")
        
        # Vectorisation Word2Vec 
        for a in argu:
            a=a.split(" ")
            sentences.append(a)
        m=Word2Vec(sentences, size=50, min_count=1, sg=1)
        #print(m)

        l=[]
        for s in sentences:
            l.append(vectoriser(s,m))
        X=np.array(l)
        
        # définition du nombre des clusters
        # createGraph(X,n)
        n_clusters=3
        
        # Recherche des centroids
        labels, clf=ecrireClustersFichier(X,sentences,n, n_clusters)
        
        
        # Représentation graphique des clusters
        print('Creating graphes...')
        creatGraphClusters(X,n_clusters,labels, clf, n)
        




