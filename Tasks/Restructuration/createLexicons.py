#Script de création de ressources d'unigrammes, bigrammes et trigrammes au format TSV à partir du corpus normalisé et formatté en XML (sample ou entier)
#Il s'utilise à la ligne de commande dans un terminal : python3 createLexicons.py corpusXML

import sys
import time
from datetime import timedelta
import csv
import spacy
import xml.etree.ElementTree

def main(XMLToParse):

	#on garde l'heure à laquelle le traitement a commencé
	start = time.time()

	#modèle Spacy
	nlp = spacy.load("fr_core_news_sm")

	#on parse l'arborescence XML
	tree = xml.etree.ElementTree.parse(XMLToParse)

	#on récupère le nom du fichier d'entrée pour générer les noms des fichiers de sorties
	fileName = XMLToParse
	outFileUni = XMLToParse.split(".")[0].split("_")[0] + "_unigrammes-corpus-id-cat.tsv"
	outFileUniH = XMLToParse.split(".")[0].split("_")[0] + "_unigrammes-corpus-human.tsv"

	outFileBi = XMLToParse.split(".")[0].split("_")[0] + "_bigrammes-corpus-id-cat.tsv"
	outFileBiH = XMLToParse.split(".")[0].split("_")[0] + "_bigrammes-corpus-human.tsv"

	outFileTri = XMLToParse.split(".")[0].split("_")[0] + "_trigrammes-corpus-id-cat.tsv"
	outFileTriH = XMLToParse.split(".")[0].split("_")[0] + "_trigrammes-corpus-human.tsv"

	#création des différents fichiers de sortie
	#pour chaque ressource une version complète avec les id et les catégories
	#et également une version lisible par les humains avec juste les formes et les occurrences
	with open(outFileUni,"w",encoding="utf-8") as uniCorp, open(outFileUniH,"w",encoding="utf-8") as uniHum, open(outFileBi,"w",encoding="utf-8") as biCorp, open(outFileBiH,"w",encoding="utf-8") as biHum, open(outFileTri,"w",encoding="utf-8") as triCorp, open(outFileTriH,"w",encoding="utf-8") as triHum, open("createLexicons.log","w",encoding="utf-8") as log:
		fileUniCorp = csv.writer(uniCorp,delimiter="\t",lineterminator="\n")
		fileUniCorp.writerow(["unigrammes","occurrences_dans_corpus","catégories","contrib_ids"])

		fileUniHum = csv.writer(uniHum, delimiter="\t",lineterminator="\n")
		fileUniHum.writerow(["unigrammes","occurrences_dans_corpus"])

		fileBiCorp = csv.writer(biCorp,delimiter="\t",lineterminator="\n")
		fileBiCorp.writerow(["bigrammes","occurrences_dans_corpus","catégories","contrib_ids"])

		fileBiHum = csv.writer(biHum,delimiter="\t",lineterminator="\n")
		fileBiHum.writerow(["bigrammes","occurrences_dans_corpus"])

		fileTriCorp = csv.writer(triCorp,delimiter="\t",lineterminator="\n")
		fileTriCorp.writerow(["trigrammes","occurrences_dans_corpus","catégories","contrib_ids"])

		fileTriHum = csv.writer(triHum,delimiter="\t",lineterminator="\n")
		fileTriHum.writerow(["trigrammes","occurrences_dans_corpus"])

		#racine xml
		root = tree.getroot()

		#les trois dictionnaires qui vont contenir les n-grammes
		dicoUni = {}
		dicoBi = {}
		dicoTri = {}

		print("Récupération du contenu du corpus et calcul des unigrammes, bigrammes et trigrammes.")

		#on parse le fichier XML
		for child in root:
			#récupération des différentes informations
			contrib = child.text
			idcont = child.attrib["id"]
			cat = child.attrib["cat"]

			#on passe chaque contribution à Spacy
			doc = nlp(contrib)
			tokens = []
			#on parcourt chaque token
			for token in doc:
				tokens.append(token.text)

				#gestion des unigrammes
				if dicoUni.get(token.text) is None:
					dicoUni[token.text] = [1, [idcont], [cat]]
				else:
					dicoUni[token.text][0]+=1
					if idcont not in dicoUni[token.text][1]:
						dicoUni[token.text][1].append(idcont)
					if cat not in dicoUni[token.text][2]:
						dicoUni[token.text][2].append(cat)

			#gestion des bigrammes
			bigrams = [tokens[i:i+2] for i in range(len(tokens)-2+1)]
			
			for bi in bigrams:
				if dicoBi.get((bi[0],bi[1])) is None:
					dicoBi[(bi[0],bi[1])] = [1,[idcont],[cat]]
				else:
					dicoBi[(bi[0],bi[1])][0]+=1
					if idcont not in dicoBi[(bi[0],bi[1])][1]:
						dicoBi[(bi[0],bi[1])][1].append(idcont)
					if cat not in dicoBi[(bi[0],bi[1])][2]:
						dicoBi[(bi[0],bi[1])][2].append(cat)

			#gestion des trigrammes
			trigrams = [tokens[i:i+3] for i in range(len(tokens)-3+1)]

			for tri in trigrams:
				if dicoTri.get((tri[0],tri[1],tri[2])) is None:
					dicoTri[(tri[0],tri[1],tri[2])] = [1,[idcont],[cat]]
				else:
					dicoTri[(tri[0],tri[1],tri[2])][0]+=1
					if idcont not in dicoTri[(tri[0],tri[1],tri[2])][1]:
						dicoTri[(tri[0],tri[1],tri[2])][1].append(idcont)
					if cat not in dicoTri[(tri[0],tri[1],tri[2])][2]:
						dicoTri[(tri[0],tri[1],tri[2])][2].append(cat)

		#écriture des sorties unigrammes
		for token, features in sorted(dicoUni.items(), key=lambda item:item[1][0],reverse=True):
			
			categories = ""
			ids = ""
			for element in features[2]:
				categories = categories + element + "/"
			for element in features[1]:
				ids = ids + element + "/"

			fileUniCorp.writerow([token,features[0],categories,ids])
			fileUniHum.writerow([token,features[0]])

		#écriture des sorties bigrammes
		for bigram, features in sorted(dicoBi.items(), key=lambda item:item[1][0],reverse=True):
			
			categories = ""
			ids = ""
			for element in features[2]:
				categories = categories + element + "/"
			for element in features[1]:
				ids = ids + element + "/"

			fileBiCorp.writerow([bigram[0]+" "+ bigram[1],features[0],categories,ids])
			fileBiHum.writerow([bigram[0]+" "+ bigram[1],features[0]])

		#écriture des sorties trigrammes
		for trigram, features in sorted(dicoTri.items(), key=lambda item:item[1][0],reverse=True):
			
			categories = ""
			ids = ""
			for element in features[2]:
				categories = categories + element + "/"
			for element in features[1]:
				ids = ids + element + "/"

			fileTriCorp.writerow([trigram[0]+" "+ trigram[1]+ " " + trigram[2],features[0],categories,ids])
			fileTriHum.writerow([trigram[0]+" "+ trigram[1]+ " " + trigram[2],features[0]])

		#durée d'éxécution du traitement
		duree = time.time() - start

		#on écrit les stats dans le fichier log
		log.write("Durée de traitement du corpus : {} secondes.\n".format(timedelta(seconds=round(duree))))
		log.write("Nombre de formes différentes d'unigrammes : {}.\n".format(len(dicoUni)))
		log.write("Nombre de formes différentes de bigrammes : {}.\n".format(len(dicoBi)))
		log.write("Nombre de formes différentes de trigrammes : {}.\n".format(len(dicoTri)))

		print("Les fichiers de ressources ont été générés dans votre répertoire courant.\nUn fichier log a également été généré.")

if __name__ == '__main__':
  main(sys.argv[1])