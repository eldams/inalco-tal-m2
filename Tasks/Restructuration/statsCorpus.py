import sys
import xml.etree.ElementTree
import spacy
import time
from datetime import timedelta

# Chargement du modèle linguistique
nlp = spacy.load("fr_core_news_sm")

# Creation des variables
dicoCat = {}
i=0

wordsDC = {}
sentsDC = {}
WordinSentDC = {}
moyWordinSentDC = {}
ponctDC = {}

wordsFDP = {}
sentsFDP = {}
WordinSentFDP = {}
moyWordinSentFDP = {}
ponctFDP = {}

wordsTE = {}
sentsTE = {}
WordinSentTE = {}
moyWordinSentTE = {}
ponctTE = {}

wordsOESP = {}
sentsOESP = {}
WordinSentOESP = {}
moyWordinSentOESP = {}
ponctOESP = {}

# Definitions des fonctions pour alleger le code

def countWords(dico,ID,content):
	"""
	On compte le nombre de tokens dans chaque contribution
	"""
	dico[ID] = len(content)

def countSents(dico,ID,content):
	"""
	On compte le nombre de phrases dans chaque contribution
	"""
	listofsents = []
	for sent in content.sents:
		listofsents.append(sent)
		x = len(listofsents)
		dico[ID] = x

def countWinS(dico,ID,content):
	"""
	On compte le nombre de tokens dans chaque phrase dans chaque contribution
	"""
	lofs = []
	for sent in content.sents:
		lofs.append(len(sent))
	dico[ID] = lofs

def countPonct(dico,ID,content):
	"""
	On compte le nombre de ponctuations fortes (.?!) dans chaque contribution
	"""
	listofponct = []
	for token in content:
		if "." in token.text or "?" in token.text or "!" in token.text:
			listofponct.append(token)
			x = len(listofponct)
			dico[ID] = x

def moyWinS(dicoIn,dicoOut):
	"""
	On compte le nombre moyen de tokens par phrase
	"""
	for k,v in dicoIn.items():
		dicoOut[k] = sum(v)/len(v)

def main(XMLtoparse):
	start = time.time()
	fileName = XMLtoparse
	outFileName = XMLtoparse.split(".")[0] + "_stats.txt"
	with open(outFileName,"w",encoding="utf-8") as FileOut, open("statsCorpus.log","w",encoding="utf-8") as log:
		# Parse XML et remplissage des dictionnaires
		root = xml.etree.ElementTree.parse(fileName).getroot()
		print("Récupération du contenu du corpus, calcul des statistiques, remplissage des dictionnaires ...\n")
		for child in root:
			contrib = child.text
			idcont = child.attrib["id"]
			cat = child.attrib["cat"]
			doc = nlp(child.text)
			# on compte le nombre de contribution par thème
			if cat not in dicoCat.keys():
				dicoCat[cat] = 1
			else:
				dicoCat[cat] += 1
			# permet d'obtenir les différents comptes de token, phrases et ponctuation pour chaque thème
			if cat == "DEMOCRATIE_ET_CITOYENNETE":
				# compte le nombre de tokens par contribution
				countWords(wordsDC,idcont,doc)
				# compte le nombre de phrases par contribution
				countSents(sentsDC,idcont,doc)
				# compte le nombre de tokens par phrase
				countWinS(WordinSentDC,idcont,doc)
				# compte le nombre de ponctuations fortes par contribution
				countPonct(ponctDC,idcont,doc)
				# moyenne du nombre de tokens par phrase
				moyWinS(WordinSentDC,moyWordinSentDC)
			if cat == "LA_FISCALITE_ET_LES_DEPENSES_PUBLIQUES":
				countWords(wordsFDP,idcont,doc)
				countSents(sentsFDP,idcont,doc)
				countWinS(WordinSentFDP,idcont,doc)
				countPonct(ponctFDP,idcont,doc)
				moyWinS(WordinSentFDP,moyWordinSentFDP)
			if cat == "LA_TRANSITION_ECOLOGIQUE":
				countWords(wordsTE,idcont,doc)
				countSents(sentsTE,idcont,doc)
				countWinS(WordinSentTE,idcont,doc)
				countPonct(ponctTE,idcont,doc)
				moyWinS(WordinSentTE,moyWordinSentTE)
			if cat == "ORGANISATION_DE_LETAT_ET_DES_SERVICES_PUBLICS":
				countWords(wordsOESP,idcont,doc)
				countSents(sentsOESP,idcont,doc)
				countWinS(WordinSentOESP,idcont,doc)
				countPonct(ponctOESP,idcont,doc)
				moyWinS(WordinSentOESP,moyWordinSentOESP)

		print("Remplissage du fichier ...\n")

		FileOut.write("Statistiques du corpus {}\n\n".format(fileName))

		FileOut.write("### DEMOCRATIE_ET_CITOYENNETE ###\n\n")

		FileOut.write("** Statistiques sur les contributions du thème **\n\n")
		
		############ TOKEN
		FileOut.write("TOKEN\n\n")
		
		# nombre de token dans DC
		nbTokDC = sum(wordsDC.values())
		FileOut.write("Nombre de tokens par contributions du thème: {}\n".format(nbTokDC))

		# moyenne des tokens dans DC
		moy = nbTokDC/len(wordsDC)
		FileOut.write("Moyenne des tokens par contributions du thème: {}\n\n".format(round(moy,2)))

		############ PHRASE
		FileOut.write("PHRASE\n\n")

		# nombre de phrase dans DC
		nbSentDC = sum(sentsDC.values())
		FileOut.write("Nombre de phrases par contributions du thème: {}\n".format(nbSentDC))

		# moyenne des phrases dans DC
		moy = nbSentDC/len(sentsDC)
		FileOut.write("Moyenne des phrases par contributions du thème: {}\n\n".format(round(moy,2)))

		############ PONCTUATION
		FileOut.write("PONCTUATION\n\n")

		# nombre de ponctuation forte dans DC
		nbPonctDC = sum(ponctDC.values())
		FileOut.write("Nombre de ponctuations fortes par contributions du thème: {}\n".format(nbPonctDC))

		# moyenne des ponctuations fortes dans DC
		moy = nbPonctDC/len(ponctDC)
		FileOut.write("Moyenne des ponctuations fortes par contributions du thème: {}\n\n".format(round(moy,2)))

		############# MOYENNE
		# FileOut.write("MOYENNE A PART\n\n")
		# FileOut.write("Moyenne des tokens par phrases du thème\n\n")
		# for k,v in moyWordinSentDC.items():
		# 	FileOut.write("La contribution {} compte {} tokens par phrase\n".format(k,round(v,2)))

		# FileOut.write("\n")

		FileOut.write("** Statistiques globales du thème **\n\n")

		moyDC = nbTokDC/4
		FileOut.write("Moyenne des tokens du thème:{}\n".format(round(moyDC,2)))

		moyDC = nbSentDC/4
		FileOut.write("Moyenne des phrases du thème: {}\n".format(round(moyDC,2)))

		moyDC = nbPonctDC/4
		FileOut.write("Moyenne des ponctuations fortes du thème: {}\n\n".format(round(moyDC,2)))

		FileOut.write("### LA_FISCALITE_ET_LES_DEPENSES_PUBLIQUES ###\n\n")

		FileOut.write("** Statistiques des contributions du thème **\n\n")

		############ TOKEN
		FileOut.write("TOKEN\n\n")

		# nombre de token dans FDP
		nbTokFDP = sum(wordsFDP.values())
		FileOut.write("Nombre de tokens par contributions du thème: {}\n".format(nbTokFDP))

		# moyenne des tokens dans FDP
		moy = nbTokFDP/len(wordsFDP)
		FileOut.write("Moyenne des tokens par contributions du thème: {}\n\n".format(round(moy,2)))

		############ PHRASE
		FileOut.write("PHRASE\n\n")

		# nombre de phrase dans FDP
		nbSentFDP = sum(sentsFDP.values())
		FileOut.write("Nombre de phrases par contributions du thème: {}\n".format(nbSentFDP))

		# moyenne des phrases dans FDP
		moy = nbSentFDP/len(sentsFDP)
		FileOut.write("Moyenne des phrases par contributions du thème: {}\n\n".format(round(moy,2)))

		############ PONCTUATION
		FileOut.write("PONCTUATION\n\n")

		# nombre de ponctuation forte dans FDP
		nbPonctFDP = sum(ponctFDP.values())
		FileOut.write("Nombre de ponctuations fortes par contributions du thème: {}\n".format(nbPonctFDP))

		# moyenne des ponctuations fortes dans FDP
		moy = nbPonctFDP/len(ponctFDP)
		FileOut.write("Moyenne des ponctuations fortes par contributions du thème: {}\n\n".format(round(moy,2)))

		############# MOYENNE
		# FileOut.write("MOYENNE A PART\n\n")
		# FileOut.write("Moyenne des tokens par phrases du thème\n\n")
		# for k,v in moyWordinSentFDP.items():
		# 	FileOut.write("La contribution {} compte {} tokens par phrase\n".format(k,round(v,2)))

		# FileOut.write("\n")

		FileOut.write("** Statistiques globales du thème **\n\n")

		moyFDP = nbTokFDP/4
		FileOut.write("Moyenne des tokens du thème: {}\n".format(round(moyFDP,2)))

		moyFDP = nbSentFDP/4
		FileOut.write("Moyenne des phrases du thème: {}\n".format(round(moyFDP,2)))

		moyFDP = nbPonctFDP/4
		FileOut.write("Moyenne des ponctuations fortes du thème: {}\n\n".format(round(moyFDP,2)))

		FileOut.write("### LA_TRANSITION_ECOLOGIQUE ###\n\n")

		FileOut.write("** Statistiques des contributions du thème **\n\n")

		############ TOKEN
		FileOut.write("TOKEN\n\n")

		# nombre de token dans TE
		nbTokTE = sum(wordsTE.values())
		FileOut.write("Nombre de tokens par contributions du thème: {}\n".format(nbTokTE))

		# moyenne des tokens dans TE
		moy = nbTokTE/len(wordsTE)
		FileOut.write("Moyenne des tokens par contributions du thème: {}\n\n".format(round(moy,2)))

		############ PHRASE
		FileOut.write("PHRASE\n\n")

		# nombre de phrase dans TE
		nbSentTE = sum(sentsTE.values())
		FileOut.write("Nombre de phrases par contributions du thème: {}\n".format(nbSentTE))

		# moyenne des phrases dans TE
		moy = nbSentTE/len(sentsTE)
		FileOut.write("Moyenne des phrases par contributions du thème: {}\n\n".format(round(moy,2)))

		############ PONCTUATION
		FileOut.write("PONCTUATION\n\n")

		# nombre de ponctuation forte dans TE
		nbPonctTE = sum(ponctTE.values())
		FileOut.write("Nombre de ponctuations fortes par contributions du thème: {}\n".format(nbPonctTE))

		# moyenne des ponctuations fortes dans TE
		moy = nbPonctTE/len(ponctTE)
		FileOut.write("Moyenne des ponctuations fortes par contributions du thème:{}\n\n".format(round(moy,2)))

		############# MOYENNE
		# FileOut.write("MOYENNE A PART\n\n")
		# FileOut.write("Moyenne des tokens par phrases du thème\n\n")
		# for k,v in moyWordinSentTE.items():
		# 	FileOut.write("La contribution {} compte {} tokens par phrase\n".format(k,round(v,2)))

		# FileOut.write("\n")

		FileOut.write("** Statistiques globales du thème **\n\n")

		moyTE = nbTokTE/4
		FileOut.write("Moyenne des tokens du thème: {}\n".format(round(moyTE,2)))

		moyTE = nbSentTE/4
		FileOut.write("Moyenne des phrases du thème: {}\n".format(round(moyTE,2)))

		moyTE = nbPonctTE/4
		FileOut.write("Moyenne des ponctuations fortes du thème: {}\n\n".format(round(moyTE,2)))

		FileOut.write("### ORGANISATION_DE_LETAT_ET_DES_SERVICES_PUBLICS ###\n\n")

		FileOut.write("** Statistiques des contributions du thème **\n\n")

		############ TOKEN
		FileOut.write("TOKEN\n\n")

		# nombre de token dans OESP
		nbTokOESP = sum(wordsOESP.values())
		FileOut.write("Nombre de tokens par contributions du thème: {}\n".format(nbTokOESP))

		# moyenne des tokens dans OESP
		moy = nbTokOESP/len(wordsOESP)
		FileOut.write("Moyenne des tokens par contributions du thème: {}\n\n".format(round(moy,2)))

		############ PHRASE
		FileOut.write("PHRASE\n\n")

		# nombre de phrase dans OESP
		nbSentOESP = sum(sentsOESP.values())
		FileOut.write("Nombre de phrases par contributions du thème: {}\n".format(nbSentOESP))

		# moyenne des phrases dans OESP
		moy = nbSentOESP/len(sentsOESP)
		FileOut.write("Moyenne des phrases par contributions du thème: {}\n\n".format(round(moy,2)))

		############ PONCTUATION
		FileOut.write("PONCTUATION\n\n")

		# nombre de ponctuation forte dans OESP
		nbPonctOESP = sum(ponctOESP.values())
		FileOut.write("Nombre de ponctuations fortes par contributions du thème:{}\n".format(nbPonctOESP))

		# moyenne des ponctuations fortes dans OESP
		moy = nbPonctOESP/len(ponctOESP)
		FileOut.write("Moyenne des ponctuations fortes par contributions du thème: {}\n\n".format(round(moy,2)))

		############# MOYENNE
		# FileOut.write("MOYENNE A PART\n\n")
		# FileOut.write("Moyenne des tokens par phrases du thème\n\n")
		# for k,v in moyWordinSentOESP.items():
		# 	FileOut.write("La contribution {} compte {} tokens par phrase\n".format(k,round(v,2)))

		# FileOut.write("\n")

		FileOut.write("** Statistiques globales du thème **\n\n")

		moyOESP = nbTokOESP/4
		FileOut.write("Moyenne des tokens du thème: {}\n".format(round(moyOESP,2)))

		moyOESP = nbSentOESP/4
		FileOut.write("Moyenne des phrases du thème: {}\n".format(round(moyOESP,2)))

		moyOESP = nbPonctOESP/4
		FileOut.write("Moyenne des ponctuations fortes du thème: {}\n\n".format(round(moyOESP,2)))


		FileOut.write("### STATISTIQUES GENERALES SUR LE CORPUS ###\n\n")

		FileOut.write("Nombre de contributions dans chaque thème du corpus: \n\n")
		for k,v in dicoCat.items():
			FileOut.write("Le thème {} compte {} contributions.\n".format(k,v))
		FileOut.write("\n")

		FileOut.write("TOTAUX\n\n")

		nbTokCorp = nbTokDC+nbTokFDP+nbTokTE+nbTokOESP
		FileOut.write("Nombre de tokens dans le corpus: {}\n".format(nbTokCorp))

		nbSentCorp = nbSentDC+nbSentFDP+nbSentTE+nbSentOESP
		FileOut.write("Nombre de phrases dans le corpus: {}\n".format(nbSentCorp))

		nbPonctCorp = nbPonctDC+nbPonctFDP+nbPonctTE+nbPonctOESP
		FileOut.write("Nombre de ponctuations fortes dans le corpus: {}\n\n".format(nbPonctCorp))


		############ DICTIONNAIRES

		# FileOut.write("-----------Dictionnaire du nombre de tokens par contribution (wordsDC)------------\n\n")
		# for k,v in wordsDC.items():
		# 	FileOut.write("{}:{}\t".format(k,v))
		
		# FileOut.write("-----------Dictionnaire du nombre de phrases par contribution (sentsDC) ------------")
		# for k,v in sentsDC.items():
		# 	FileOut.write("{}:{}\t".format(k,v))

		# FileOut.write("-----------Dictionnaire du nombre de ponctuations fortes par contribution (ponctDC) ------------")
		# for k,v in ponctDC.items():
		# 	FileOut.write("{}:{}\t".format(k,v))

		# FileOut.write("-----------Dictionnaire du nombre de tokens par phrase par contribution (WordinSentDC) ------------")
		# for k,v in WordinSentDC.items():
		# 	FileOut.write("{}:{}\t".format(k,v))

		# FileOut.write("\n\n")

		# FileOut.write("-----------Dictionnaire du nombre de tokens par contribution (wordsFDP) ------------")
		# for k,v in wordsFDP.items():
		# 	FileOut.write("{}:{}\t".format(k,v))

		# FileOut.write("-----------Dictionnaire du nombre de phrases par contribution (sentsFDP) ------------")
		# for k,v in sentsFDP.items():
		# 	FileOut.write("{}:{}\t".format(k,v))

		# FileOut.write("-----------Dictionnaire du nombre de ponctuations fortes par contribution (ponctFDP) ------------")
		# for k,v in ponctFDP.items():
		# 	FileOut.write("{}:{}\t".format(k,v))

		# FileOut.write("-----------Dictionnaire du nombre de tokens par phrase par contribution (WordinSentFDP) ------------")
		# for k,v in WordinSentFDP.items():
		# 	FileOut.write("{}:{}\t".format(k,v))

		# FileOut.write("\n\n")

		# FileOut.write("-----------Dictionnaire du nombre de tokens par contribution (wordsTE) ------------")
		# for k,v in wordsTE.items():
		# 	FileOut.write("{}:{}\t".format(k,v))

		# FileOut.write("-----------Dictionnaire du nombre de phrases par contribution (sentsTE) ------------")
		# for k,v in sentsTE.items():
		# 	FileOut.write("{}:{}\t".format(k,v))

		# FileOut.write("-----------Dictionnaire du nombre de ponctuations fortes par contribution (ponctTE) ------------")
		# for k,v in ponctTE.items():
		# 	FileOut.write("{}:{}\t".format(k,v))

		# FileOut.write("-----------Dictionnaire du nombre de tokens par phrase par contribution (WordinSentTE) ------------")
		# for k,v in WordinSentTE.items():
		# 	FileOut.write("{}:{}\t".format(k,v))

		# FileOut.write("\n\n")

		# FileOut.write("-----------Dictionnaire du nombre de tokens par contribution (wordsOESP) ------------")
		# for k,v in wordsOESP.items():
		# 	FileOut.write("{}:{}\t".format(k,v))

		# FileOut.write("-----------Dictionnaire du nombre de phrases par contribution (sentsOESP) ------------")
		# for k,v in sentsOESP.items():
		# 	FileOut.write("{}:{}\t".format(k,v))

		# FileOut.write("-----------Dictionnaire du nombre de ponctuations fortes par contribution (ponctOESP) ------------")
		# for k,v in ponctOESP.items():
		# 	FileOut.write("{}:{}\t".format(k,v))

		# FileOut.write("-----------Dictionnaire du nombre de tokens par phrase par contribution (WordinSentOESP) ------------")
		# for k,v in WordinSentOESP.items():
		# 	FileOut.write("{}:{}\t".format(k,v))

		# FileOut.write("\n\n")

		print("Le fichier de statistiques a été généré dans votre répertoire courant, son nom est : {}\n".format(outFileName))

		duree = time.time() - start
		log.write("Durée de traitement du corpus : {} secondes.\n".format(timedelta(seconds=round(duree))))

if __name__ == '__main__':
  main(sys.argv[1])
