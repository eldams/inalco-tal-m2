# Restructuration
*Clémence et Lucie*

## Commandes pour lancer les différents scripts

| :warning: Les scripts mettent du temps à s'exécuter, regardez les fichiers log pour plus d'informations|
|--------------------------------------------------------------------------------------------------------|

| Nom du script       | Commande à effectuer                       |
| :-----------------: | :----------------------------------------: |
| *fromRawtoXml.py*   | `python3 fromRawtoXml.py nomducorpus`      |
| *createLexicons.py* | `python3 createLexicons.py nomducorpusXML` |
| *statsCorpus.py*    | `python3 statsCorpus.py nomducorpusXML`    |

## Entrée
Texte brut (sample pour tester puis corpus entier)

## Sortie 
Document structuré au format XML 

## Traitements opérés sur le corpus

* Un nettoyage du corpus
* Une structuration XML du corpus
* Une génération de ressources
* Une génération de statistiques

### Nettoyage du corpus

Nous avons voulu mettre en place un nettoyage minimal du corpus en se concentrant davantage sur la syntaxe de la ponctuation et des majuscules car le but n'est pas de nettoyer entièrement le corpus, mais de pouvoir donner à des outils d'analyse linguistique (étiqueteur en entités nommées ou en parties-du-discours par exemple) une entrée relativement propre et facilement parsable. Ainsi, nous avons accentué notre travail sur cette syntaxe de la ponctuation et des majuscules qui n'était pas homogène à travers le corpus. Bien entendu, il n'est pas possible de corriger l'orthographe du corpus de cette manière et celle-ci a été préservée. Par ailleurs, ce ne serait pas souhaitable, l'orthographe pouvant également révéler une certaine classe sociale de la population ou encore retranscrire certaines émotions comme l'énervement.

L'objectif de ce nettoyage est donc de pouvoir rendre plus lisible ce corpus par d'autres outils et le rendre également plus uniforme, tout en gardant au maximum son format et contenu original afin de ne pas biaiser la suite du projet.

Par ailleurs, nous avions remarqué que le corpus contenait des doublons. Nous avons donc mis en place un nettoyage de ces doublons afin de ne pas avoir une même contribution plusieurs fois au sein du corpus.

#### Normalisation de la syntaxe de la ponctuation ####

* On corrige les espaces au niveau des parenthèses : un espace avant "(" et un espace après ")", on retire les espaces à l'intérieur des parenthèses
> Par exemple, `publique(` devient `publique (`, `)à l'empilement` devient `) à l'empilement` et `( renault )` devient `(renault)`
* On retire les espaces avant une virgule ou un point et on ajoute un espace après une virgule ou un point
> Par exemple, `fonction publique ,` ou `creation d'entreprises .` deviennent respectivement `fonction publique,` et `creation d'entreprises.` et `contribuer,les salariés` ou `convaincante.Il` deviennent `contribuer, les salariés` et `convaincante. Il`
* On met un espace avant et après cette ponctuation forte double : `:!?`
> Par exemple, `énergie:nucléaire` devient `énergie : nucléaire`, `voiture!Le` devient `voiture ! Le` et `coût?Le` devient `coût ? Le`
* On ajoute une apostrophe entre une consonne toute seule et une autre chaîne de caractères commençant par une voyelle
> Par exemple, `point d indice` devient `point d'indice`
* Dans les listes avec tirets, on ajoute un espace après le tiret (en faisant attention à ne pas ajouter cet espace pour des mots composés ou des formes composées)
> Par exemple, `-Exemple Si APL` devient `- Exemple Si APL` mais `ces gens-là` et `sous-payés` restent pareils
* A la fin de chaque contribution, on ajoute un point (.) si la contribution ne se termine ni pas un point (.), ni par un point d'interrogation (?), ni par un point d'exclamation (!)
> Par exemple, `(d'où un problème de progressivité)` devient `(d'où un problème de progressivité).`
* Quand une virgule est immédiatement suivie par un point, on remplace cette suite par un point seul
> Par exemple, `lourdement taxés,.` devient `lourdement taxés.`
* On corrige les guillemets non conventionnelles ainsi que les guillemets françaises en normalisant à l'aide des guillemets doubles (") ou simples (')
> Par exemple, `‘'philosophie''` devient `"philosophie"` et `« la résistance à l'oppression »` devient `"la résistance à l'oppression"`
* On retire également les espaces à l'intérieur des guillemets
> Par exemple, `" Tarification progressive de la consommation d'énergie "` devient `"Tarification progressive de la consommation d'énergie"`
* On normalise les espaces multiples en les remplaçant par un espace simple
> Par exemple, `mis en place  mais` devient `mis en place mais`
* On retire les espaces qui peuvent exister entre ponctuations fortes qui se suivent
> Par exemple, `? ? ?`, `! !` ou `. . . .` deviennent `???`, `!!!` ou `....`
* On enlève l'espace à la fin de chaque contribution (car on ajoute un espace après les ponctuations fortes)

#### Normalisation des majuscules ####

* On met une majuscule à chaque début de contribution
> Par exemple, `diminuer les salaires` devient `Diminuer les salaires`
* On met une majuscule également après les points (.), points d'interrogation (?) et d'exclamation (!)
> Par exemple, `.le solaire` devient `. Le solaire`, `? la peur` devient `? La peur`, et `! diminuer` devient `! Diminuer`
* On corrige la majuscule sur le nom propre "France"
> `france` devient `France`

#### Remarques concernant ce nettoyage ####

Nous avons normalisé la ponctuation seulement quand cela ne perdait pas l'émotion transmise dans les contributions comme par exemple une longue suite de ponctuations fortes (......., ????, !!!!) ou encore des phrases entières en majuscules. Par ailleurs, les suites de tirets (-) et les marqueurs de listes n'ont pas été retirés du corpus car tous ces éléments participent à sa structuration et sont également en eux-mêmes des marqueurs d'argumentation dans un sens puisque la personne ayant écrit sa contribution a délibérement structuré sa participation dans le but de démontrer une idée, soutenir une thèse ou encore tout simplement énoncer des faits par exemple.

##### Quelques statistiques sur ce nettoyage et cette normalisation du corpus :
 
* 14 570 contributions doublons on été retirées du corpus sur 342 097 contributions du corpus original, ce qui représente une élimination de 4,26 % des contributions
* 134 154 contributions ont été normalisées et nettoyées au niveau de la ponctuation, soit 39,22 % du corpus entier
* 175 370 contributions ont été normalisées et nettoyées au niveau des majuscules, soit 51,26 % du corpus entier

### Structuration XML du corpus

Nous avons modélisé une structure simple permettant de relier chaque contribution à son thème/sa catégorie : 
* Une racine "corpus" qui contient l'ensemble des contributions
* Un élément "contrib" qui contient le texte de chaque contribution
* Cet élément "contrib" a quatre attributs : 
	* "id" pour son identifiant unique dans le corpus
	* "cat" pour le thème/la catégorie à lequel/laquelle elle appartient
	* "start" pour l'éventuel texte juste avant la contribution, par exemple `Citoyen / Citoyenne`
	* "nbTokens" pour le nombre de tokens de la contribution

Nous n'avons pas organisé le document XML par thème/catégorie car nous avons voulu faire une structure très simple pour faciliter les post-traitements. Ainsi, avec notre modélisation, il est très simple de retrouver la catégorie de chaque contribution sans avoir à naviguer dans l'arborescence du document.

### Remarques générales

Nous avons gardé toutes les contributions sans enlever celles trop courtes ou trop longues car c'est un choix totalement arbitraire et nous avons constaté que certaines contributions courtes pouvaient contenir un peu d'argumentation. Par ailleurs, ces contributions courtes peuvent permettre à un chatbot d'avoir un ton plus polémique qu'avec des contributions de taille moyenne et c'est pour cela également que nous avons décidé de les laisser. Il aurait été très arbitraire de retirer du corpus toutes les contributions ne dépassant pas la longueur de trois mots par exemple. Notre choix a donc été de les laisser, afin que les autres groupes de travail puissent utiliser des contributions qui peuvent s'avérer utiles. Quant aux contributions très longues, elles sont assez lourdes mais elles sont très bien structurées pour la plupart et les enlever pourrait nous faire perdre de l'argumentativité. Par ailleurs, leur structure même relève de l'argumentation dans le corpus. Enfin, nous avions pensé à un moment de séparer en plusieurs contributions ces longues contributions structurées mais ne l'avons pas fait car ce choix aurait été arbitraire et peu linguistique, et enfin il aurait été possible de séparer en deux une séquence argumentative importante, ce qui n'est pas souhaitable dans le cadre de ce projet où l'enchaînement des différentes phrases est à préserver. S'il est nécessaire, dans les tâches suivantes, de filtrer les contributions par rapport à leur taille, il est possible de le faire grâce à l'attribut nbTokens présent dans la structure XML.

### Génération de ressources

Après avoir structuré, normalisé et nettoyé le corpus, nous avons décidé de générer quelques ressources lexicales (à base de N-grammes) afin de pouvoir visualiser un peu le contenu du corpus et pouvoir fournir aux autres groupes du projet quelques ressources éventuelles. Nous avons voulu constater si des mots ou expressions étaient beaucoup plus utilisés que d'autres dans ce corpus du Grand Débat National et de pouvoir voir si un ton ou un registre particulier était plus présent qu'un autre. Notre objectif était également de pouvoir constater si les thèmes des contributions ressortaient à travers le vocabulaire de ces lexiques et si des marqueurs de discours ou d'argumentation pouvaient être repérés dedans. Repérer ces marqueurs pourrait être intéressant d'une part pour comprendre ceux qui sont le plus utilisés dans le corpus et pour avoir par la suite une liste conséquente (un lexique) à utiliser dans d'autres tâches ou applications, comme pour un chatbot par exemple où un lexique de ce genre permettrait d'enchaîner des arguments ou des énoncés. 

Nous nous sommes concentrées sur 3 types de N-grammes : des unigrammes, bigrammes et trigrammes. Les unigrammes permettent d'avoir une vision globale du corpus et permettent également de pouvoir dresser ensuite une liste de mots vides ou de mots outils et des ponctuations afin de les mettre de côté si l'on souhaite. Les bigrammes et trigrammes permettent quant à eux de repérer des expressions et peuvent également servir pour réperer des patrons morphosyntaxiques à extraire par exemple. Par ailleurs, ces ressources nous donnent des suites de tokens et nous informent donc du contexte d'utilisation de certains mots voire peuvent nous indiquer certains usages de mots précis selon les auteurs des contributions et ainsi peuvent nous donner accès d'une certaine façon aux opinions de ces différentes personnes.

Nous avons décidé de générer trois ressources: les unigrammes, bigrammes et trigrammes du corpus en tokenisant ce dernier. Chaque ressource est disponible selon deux formats tsv, le premier comprenant tous les identifiants des contributions comprenant l'unigramme, le bigramme ou le trigramme, ainsi que les catégories de ces contributions. Ce format n'étant pas très lisible, nous en avons fait une deuxième version plus simple à lire pour l'humain.

La génération de ces ressources prend beaucoup de temps et surtout beaucoup de mémoire. Nos machines ne nous ont pas permis de travailler sur le corpus entier. Nous avons donc décider de travailler sur la moitié du corpus en ne prenant qu'une contribution sur deux (`GDN_struct_half.xml`). Pour créer ce nouvel échantillon, nous avons utilisé la commande suivante : `cat GDN_struct.xml | awk '{if(NR%2==0) print $0}' > GDN_struct_half.xml`

Le format tsv avec les identifiants (qui se termine par `-corpus-id-cat.tsv`) affiche les N-grammes, le nombre d'occurrences du N-gramme dans le corpus, les catégories dans lesquelles le N-gramme apparaît ainsi que la liste des identifiants des contributions dans lesquelles le N-gramme apparaît.

Le format tsv lisible pour les humains (qui se termine par `-corpus-human.tsv`) affiche la listes des N-grammes et leur nombre d'occurrences dans le corpus.

L'affichage se fait par ordre décroissant du nombre d'occurrences dans les deux formats.

Les ressources sont disponibles dans le répertoire `Ressources`.

#### Remarques sur ces ressources

Nous avons voulu générer ces ressources afin d'avoir un aperçu du corpus, il s'agit plutôt d'une démarche expérimentale afin de voir si des motifs pouvaient ressortir de ce corpus. Même si les ressources obtenues ont été générées à partir de la moitié du corpus normalisé et nettoyé (soit 163 763 contributions), celles-ci nous donnent déjà un bon aperçu et la tendance générale du corpus.

##### Quelques remarques concernant les unigrammes obtenus

* 161 687 unigrammes différents ont été trouvés dans la moitié du corpus
* Les unigrammes les plus fréquents sont ceux des mots vides ou mots outils, ainsi que ceux de la ponctuation
* Plus de la moitié de ces unigrammes sont des hapax (il n'apparaissent qu'une seule fois dans ce corpus) et la majorité de ces formes présente de très faibles nombres d'occurrences par rapport au nombre de contributions
* Les formes les plus fréquentes notables sont présentes dans les quatre thèmes du corpus
* Parmi les formes très fréquentes, nous pouvons repérer les différents thèmes du corpus à travers l'usage de certains champs lexicaux et ainsi voir les principales préoccupations des français, en voici quelques exemples :
	* `France`, `pays`, `état`, `citoyens`, `français`, `fonctionnaires`, `démocratie`, `pouvoir`, `élus`, `politique`, `loi` et `gouvernement` correspondent à des discussions sur le système politique français par exemple
	* `entreprises`, `travail`, `impôts`, `dépenses`, `argent`, `retraite`, `aides`, `revenus`, `taxes`, `emploi`, `salaires`, `euros`, `production`, `coût`, `économie` et `consommation` relèvent de questions sociétales économiques, qui sont au coeur de l'actualité en France depuis quelques années
	* `écologique`, `environnement`, `transports`, `pollution` et `déchets` montrent des préoccupations d'ordre écologiques et environnementales, également au coeur de notre actualité française
	* `sécurité` est également bien représenté dans le corpus, ainsi que l'adverbe `trop` qui exprime quelque chose d'excessif
	* Enfin, on remarque que les pronoms personnels de première personne singulier et pluriel `je` et `nous` sont également très présents dans ce corpus, on comprend donc que les contributions rédigées sont très personnelles et que les français donnent leur avis et expriment leurs opinions et revendications personnelles dans ce corpus du Grand Débat National

##### Quelques remarques concernant les bigrammes obtenus

* 2 020 294 bigrammes différents ont été trouvés dans la moitié du corpus
* Là encore, les bigrammes les plus fréquents sont ceux de mots vides ou de mots outils et énormément de bigrammes sont des hapax ou présentent des occurrences très faibles par rapport au nombre de contributions
* Les formes les plus fréquentes notables sont également présentes dans les quatre thèmes du corpus
* Parmi les formes les plus fréquentes, on peut repérer ces quelques bigrammes d'exemples : 
	* `en France`, `la France`, `les citoyens`, `la population`, `services publics`, `hauts fonctionnaires`, `fonction publique` et `la République` qui concernent le système politique français et la société française
	* `les entreprises`, `les dépenses`, `l'argent`, `l'impôt`, `des impôts`, `l'ISF` et `la TVA` qui relèvent de l'économie et des finances
	* `transition écologique`, `l'environnement`, `l'écologie` qui représentent les préoccupations environnementales des Français
	* `par exemple`, `alors que`, `plutôt que`, `mais aussi` et `afin de` qui sont des exemples de marqueurs discursifs et argumentatifs, nous voyons alors que les bigrammes pourraient déjà à servir à obtenir une certaine liste de ces marqueurs
	* `doivent être`, `doit être` et `devraient être` qui nous montrent l'utilisation massive du verbe `devoir` dans le corpus et ainsi qui nous indiquent que les auteurs des contributions demandent fermement des changements et expriment fermement leurs revendications

##### Quelques remarques concernant les trigrammes obtenus

* 6 829 775 trigrammes différents ont été trouvés dans la moitié du corpus
* Il y a très peu de trigrammes fréquents par rapport au nombre de contributions et cela est logique puisque l'association de trois tokens devient plus rare à retrouver dans un corpus qu'un seul token ou que deux tokens utilisés dans le même contexte
* On peut tout de même repérer ces quelques trigrammes d'exemples :
	* `la transition écologique`, `de l'environnement`, `transports en commun`, `les énergies renouvelables` et `la voiture électrique` qui montrent l'intérêt des Français pour les questions environnementales et écologiques
	* `pouvoir d'achat`, `train de vie`, `faire des économies`, `la dépense publique` et `la fraude fiscale` qui révèlent les préoccupations d'ordre économique en France
	* `de la République`, `de la France`, `la fonction publique` et `des services publics` qui relèvent du champ lexical de l'Etat français
	* `mise en place`, `mettre en place`, `qu'il faut`, `Le Gouvernement doit` et `ne devrait pas` qui démontrent encore une fois l'utilisation d'expressions expriment le devoir et l'obligation de faire quelque chose, de changer, d'évoluer ou encore qui indique une démarche à ne pas suivre
	* `Je pense que` qui montre que les auteurs des contributions donne leur avis personnel
	* `Pourquoi ne pas` qui montre également un certain questionnement des Français vis-à-vis de l'Etat, du gouvernement

### Génération de statistiques

Nous avons décidé de faire des statistiques globales sur le corpus pour dégager un peu sa structure et mettre des chiffres plus parlant sur ce gros volume de données. Puisque nous avons également généré des ressources lexicales, nous n'avons pas voulu faire de statistiques plus précises ici pour que ce ne soit pas redondant avec les observations faites grâce aux ressources.

Une des statistiques qui n'est pas affichée dans le fichier `GDN_struct_stats.txt` est le nombre de phrases par contribution et le nombre moyen de tokens par phrases par contributions. Nous avons essayé de faire un "joli" affichage avec des phrases explicatives mais c'était vraiment trop lourd et nous avons essayé avec l'affichage du dictionnaire directement mais là non plus ce n'était pas très lisible. Tous les dictionnaires que nous avons remplis à partir du corpus et utilisés pour calculer les statistiques peuvent être consultés. Il suffit de les décommenter dans le script `statsCorpus.py` et de s'armer de patience (*environ 3h*) pour obtenir les résultats.

##### Quelques remarques concernant les statistiques obtenues

Le corpus se divise en 4 thèmes composés de nombreuses contributions. Ces 4 thèmes sont `DEMOCRATIE_ET_CITOYENNETE`, `LA_FISCALITE_ET_LES_DEPENSES_PUBLIQUES`, `LA_TRANSITION_ECOLOGIQUE` et `ORGANISATION_DE_LETAT_ET_DES_SERVICES_PUBLICS`.
Le corpus compte 32 528 739 tokens dont 1 396 546 ponctuations fortes (`.?!`), 1 590 972 phrases et 327 527 contributions.

Attention ici, nous utilisons les ressources unigrammes, bigrammes et trigrammes pour illustrer nos hypothèses et essayer de dégager une tendance générale du corpus. Ces ressources ne sont pas complètes car elles ont été générées seulement sur la moitié du corpus. Elles reflètent néanmoins une certaine tendance dans le corpus.

D'après les différentes statistiques calculées à partir du corpus entier (`GDN_struct_stats.txt`), nous observons que la catégorie qui compte le plus de contributions est `LA_FISCALITE_ET_LES_DEPENSES_PUBLIQUES` avec 127 846 contributions. S'en suit ensuite la catégorie `LA_TRANSITION_ECOLOGIQUE` avec 105 008 contributions. Les deux autres catégories sont moitié moins fournies. Cela nous permet de faire l'hypothèse suivante : les citoyens français sont plus concernés par les dépenses publiques de l'état ainsi que par l'écologie.
Dans un contexte comme celui des dernières années et notamment l'année dernière avec les nombreuses manifestations par rapport à la hausse du nombre de taxes, il est compréhensible de retrouver un nombre important de contributions sur ce sujet. Nous pouvons d'ailleurs compléter cela avec l'apparition du token `entreprises` dans la ressource des unigrammes avec 19 261 occurrences (88ème position) suivi de près par les tokens `travail`, `impôts` et `dépenses`.
En ce qui concerne l'écologie, depuis plusieurs années maintenant voire une décennie, nous sommes mis en garde et prenons de plus en plus conscience de l'impact lourd de l'Homme sur notre planète. Nous constatons à travers ces statistiques que les citoyens français sont concernés par les moyens mis en œuvre pour alléger cet impact sur notre planète. Nous pouvons également appuyer cette hypothèse sur les trois ressources qui compte 13 292 occurrences de `écologique`, 106 occurrences de `agroécologie` et de nombreux autres tokens associés, 7 521 occurrences de `transition écologique`, 474 occurrences de `transition écologique doit`.

Si nous regardons de plus près les ressources avec les identifiants (qui finissent par `-corpus-id-cats.tsv`) nous pouvons constater que ces tokens ne sont pas propres au thème `LA_TRANSITION_ECOLOGIQUE`. Ils sont présents dans les quatre thèmes. Nous observons donc que les évènements récents ont influés sur l'état d'esprit des citoyens français qui se sentent concernés par cette question. Comme dit plus haut, nous nous basons ici seulement sur des ressources à moitié complètes. Il serait intéressant de refaire ces hypothèses en nous appuyant sur les ressources complètes faites à partir du corpus entier.

Regardons maintenant plus en détails les statistiques pour chaque thème.
Pour chaque thème nous avons calculé son nombre total de tokens, de phrases et de ponctuations fortes. Vous pouvez retrouver le nombre de contributions par thème ci-dessus ou dans la rubrique `STATISTIQUES GENERALES SUR LE CORPUS` dans le fichier `GDN_struct_stats.txt`. Quand nous parlons de nombre de tokens, cela correspond à tous les mots et ponctuations présents dans chaque contribution. Nous ne parlons pas ici de vocabulaire, cela correspondrait au comptage unique de chaque tokens (sans compter les doublons).

Le thème qui compte le plus grand nombre de tokens est `LA_FISCALITE_ET_LES_DEPENSES_PUBLIQUES` avec plus de 12 millions de tokens. Cela correspond à un peu moins d'1/3 du nombre total de tokens dans le corpus. 
Nous pouvons remarquer que le nombre de phrases dans chaque thème équivaut à un peu moins de la moitié du nombre de tokens de chaque thème.
Nous remarquons également pour chaque thème que le nombre de phrases et le nombre de ponctuations fortes sont très proches (phénomène notamment dû à la normalisation que nous avons faite pour qu'il y ait un point à la fin de chaque phrase quand il n'y en avait pas). Cela nous permet de voir que la majorité des contributions se termine soit par un point (`.`), soit par un point d'interrogation (`?`) soit par un point d'exclamation (`!`). Nous pouvons supposer que l'utilisation de ces deux derniers dans ce corpus peut exprimer une incompréhension, un étonnement, une affirmation forte voire un ordre, une mise en garde ou encore de la colère.
Pour confirmer nos hypothèses nous nous appuyons sur les unigrammes, bigrammes et trigrammes générés. En effet, nous constatons que le point compte 506 388 occurrences, le point d'exclamation 60 994 occurrences et le point d'interrogation 41 606 occurrences. Nous trouvons en 6ème position des trigrammes, la suite de `!!!` avec 10 715 occurrences. Nous trouvons également la suite de `???` avec 3 074 occurrences. Nous trouvons aussi `! Le Gouvernement` avec 952 occurrences ou encore `! Il faut` avec 727 occurrences qui peuvent montrer une certaine incompréhension face à un fait décrit en amont suivi d'une proposition de la personne qui écrit. Nous retrouvons également plusieurs associations de `!` et de `vitesse` ou `radar` qui peuvent nous indiquer le mécontentement des usagers face aux limitations de vitesse et leur contrôle. Nous trouvons ensuite moins de 100 occurrences pour `l'exemple !`, `la France !` et `l'état !` qui peuvent indiquer des ordres ou des affirmations fortes, des convictions ou encore des croyances que les citoyens souhaitent exprimer. Nous retrouvons également des questionnements de la part des citoyens sous la forme de trigrammes `en France ?` avec 229 occurrences ou encore `en priorité ?` avec 130 occurrences ou encore `. Pourquoi ?` avec 121 occurrences. Parfois le questionnement est plus précis : `transition écologique ?` ou `l' environnement ?` ou `l'argent ?` avec moins de 100 occurrences.
Cela nous permet d'avoir un ressentit général sur le corpus par rapport à la situation dans laquelle se trouve les citoyens français.

## Scripts produits

### Script de nettoyage et de structuration

Il s'agit du script `fromRawtoXml.py` qui se lance à la ligne de commande de cette manière :
`python3 fromRawtoXml.py nomducorpus`

Ce script fonctionne aussi bien sur le corpus entier du Grand Débat National (`GDN.txt`) que sur vos échantillons de test. Il donne en sortie un fichier XML avec le corpus structuré et normalisé, le nom du fichier est celui de votre fichier d'entrée suivi de `_struct.xml`. Un peu plus d'une heure trente sont nécessaires pour appliquer ce script au corpus GDN entier. La sortie XML pour ce corpus est disponible ici dans ce répertoire sous le nom de `GDN_struct.xml`. N'hésitez pas à regarder le code commenté. Le script génère un fichier log (`fromRawToXml.log`) qui contient quelques statistiques et la durée du traitement.

### Script de génération de ressources

Il s'agit du script `createLexicons.py` qui se lance à la ligne de commande de cette manière :
`python3 createLexicons.py nomducorpusXML`

Ce script a été lancé sur la moitié du corpus du Grand Débat National normalisé, nettoyé et structuré au format XML (`GDN_struct.xml`). Il peut être testé également sur différents échantillons du corpus. Il donne en sortie 6 fichiers TSV (2 fichiers TSV par N-grammes) : ceux qui finissent par `-corpus-id-cat.tsv` sont destinés à un traitement informatique et ceux qui finissent par `-corpus-human.tsv` sont destinés à la lecture humaine simplifiée. Plus de quatre heures sont nécessaires pour appliquer ce script à la moitié du corpus GDN. Les sorties obtenues par ce script sont disponibles dans le répertoire `Ressources`. Ce script est également commenté dans le détail. Enfin, il génère un fichier log (`createLexicons.log`) qui contient quelques statistiques ainsi que la durée du traitement.

### Script de génération de statistiques

Il s'agit du script `statsCorpus.py` qui se lance à la ligne de commande : 
`python3 statsCorpus.py nomducorpusXML`

Ce script fonctionne aussi bien sur le corpus entier du Grand Débat National normalisé, nettoyé et structuré au format XML (`GDN_struct.xml`) que sur vos échantillons de test. Il fournit en sortie un fichier texte, le nom du fichier est celui de votre fichier d'entrée suivi de `_stats.txt`. N'hésitez pas à regarder le code commenté. Le script génère également un fichier log (`statsCorpus.log`) qui contient la durée du traitement. Plus de trois heures sont nécessaires pour appliquer ce script au corpus `GDN_struct.xml` entier. La sortie texte des statistiques de ce corpus est disponible dans le répertoire `Statistiques`.