#Script de structuration et de nettoyage du corpus GDN
#Il s'utilise à la ligne de commande dans un terminal : python3 fromRawtoXml.py nomducorpus

import sys
import time
from datetime import timedelta
import re
import spacy
from lxml import etree

def correctPunct(contribution):
	"""Prend en entrée une contribution et corrige toute la syntaxe de la ponctuation et des espaces, retourne la contribution traitée"""

	#on ajoute un espace entre le texte et (
	correct = re.sub(r"(\w)\(",r"\1 (",contribution)
	#on ajoute un espace entre ) et le texte
	correct = re.sub(r"\)(\w)",r") \1",correct)
	#on enlève les espaces à l'intérieur des parenthèses
	correct = re.sub(r"(?:(\() | (\)))",r"\1\2",correct)

	#on enlève l'espace avant virgule et point
	correct = re.sub(r" ([,.])",r"\1",correct)
	#on ajoute un espace après virgule et point
	correct = re.sub(r"([,.])(\w)", r"\1 \2",correct)

	#mettre un espace avant et après la ponctuation forte
	correct = re.sub(r"([:?!])",r" \1 ",correct)

	#ajouter une apostrophe entre une consonne toute seule et le reste si voyelle après
	correct = re.sub(r"\b([bBcCdDfFgGhHjJkKlLmMnNpPqQrRsStTvVwWxXzZ])\b ([aAàÀâÂäÄeEéÉèÈêÊëËiIîÎïÏoOôÔöÖuUüÜûÛyYœŒæÆ])",r"\1'\2",correct)

	#ajouter un espace après un tiret dans les listes
	correct = re.sub(r"(\W-|^-)(\w+) ",r"\1 \2 ",correct)

	#ajouter un point à la fin du contribution s'il n'y a rien
	if correct.strip().endswith(".") is False and correct.strip().endswith("!") is False and correct.strip().endswith("?") is False:
		correct+="."

	#quand on a une virgule suivie d'un point, on remplace par un point
	correct =  re.sub(r",\.",r". ",correct)

	#correction des guillemets non conventionnelles
	correct = re.sub(r"‘",r"'",correct)
	#on corrige deux guillemets simples qui se suivent par une guillemet double
	correct = re.sub(r"''",r'"',correct)
	#on corrige les guillemets françaises par les guillemets anglaises
	correct = re.sub(r"[«»]",r'"',correct)

	#on corrige les éventuels espaces à l'intérieur des guillemets
	correct = re.sub(r"([\"\'])\s?(\w+)\s?([\"\'])",r"\1\2\3",correct)
	
	#on corrige les éventuels espaces multiples
	correct = re.sub(r"\s+",r" ",correct)

	#on corrige les éventuels espaces entre ponctuations fortes qui se suivent
	correct = re.sub(r"([.!?])\s",r"\1",correct)
	correct = re.sub(r"([.!?])(\w)",r"\1 \2",correct)

	#on enlève l'espace à la fin de chaque contribution
	correct = correct.strip()

	return correct

def correctMaj(contribution):
	"""Prend en entrée une contribution et corrige les majuscules, retourne la contribution traitée"""

	#on met une majuscule au début de chaque contribution
	correct = contribution[0].upper() + contribution[1:]
	#on met une masjuscule après les points .!?
	correct = re.sub(r"([.!?]) ([a-z])",lambda m: m.expand(r"\1 ") + m.expand(r"\2").upper(), correct)
	#on corrige la majuscule sur France
	correct = re.sub(r"\bfrance\b",r"France",correct)

	return correct

def main(textToParse):
	#on garde l'heure à laquelle le traitement a commencé
	start = time.time()
	nlp = spacy.load("fr_core_news_sm")
	fileName = textToParse
	outFileName = textToParse.split(".")[0] + "_struct.xml"
	with open(textToParse, "r", encoding="utf-8") as FileIn, open("fromRawtoXml.log","w",encoding="utf-8") as log:
		compteurId = 0
		compteurTotalContrib = 0

		#compteurs pour le fichier log
		#compter les doublons et sauvegarder les contributions pour vérifier si doublons
		compteurDoublon = 0
		listeContribs = {}
		#compter le nombre de contributions normalisées
		compteurNormPunct = 0
		compteurNormMaj = 0

		#une racine corpus
		root = etree.Element("corpus")
		
		print("Récupération du contenu du corpus, nettoyage et structuration en cours ...\n")

		for line in FileIn:
			compteurTotalContrib+=1
			#Récupération des contenus

			#la catégorie à laquelle on enlève .csv
			categorie = line.strip().split(".")[0]
			#les éventuels mots avant la vraie contribution
			introContribCit = line.strip().split("|")[0].split(",")[-1]
			#la contribution elle-même
			contrib = line.strip().split("|")[-1]

			#on ne garde pas les contributions doublons
			if listeContribs.get(contrib) is not None:
				compteurDoublon+=1
				continue
			#on ajoute chaque contribution à un dictionnaire pour vérifier ensuite et ne pas garder les doublons
			listeContribs[contrib] = 1

			#Nettoyage

			#on corrige la ponctuation, la syntaxe etc de la contribution
			contribPropre = correctPunct(contrib)
			#on regarde si la contribution a été effectivement corrigée, si oui, on la compte
			if contribPropre!=contrib:
				compteurNormPunct+=1

			#et on corrige les majuscules
			contribPropre = correctMaj(contribPropre)
			#et on regarde si la contribution a été effectivement corrigée, si oui, on la compte
			if contribPropre!=contrib:
				compteurNormMaj+=1

			#on récupère le nombre de tokens par contribution
			doc = nlp(contribPropre)
			nbTokContrib = len(doc)

			#Structure XML

			#un élément contrib
			contribu = etree.SubElement(root,"contrib")
			#avec un attribut id (numéro unique)
			contribu.set("id", str(compteurId))
			#un attribut cat (sa catégorie)
			contribu.set("cat", categorie)
			#un attribut start (les éventuels mots avant la contribution)
			contribu.set("start", introContribCit)
			#un attribut nbTokens (nombre de tokens dans la contributions)
			contribu.set("nbTokens",str(nbTokContrib))
			#la contribution à l'intérieur des balises contrib
			contribu.text = contribPropre

			#incrémentation du compteur
			compteurId+=1

		#on récupère l'arborescence et on écrit dans un fichier
		tree = etree.ElementTree(root)
		tree.write(outFileName, encoding="utf-8",xml_declaration=True, method="xml",pretty_print=True)

		#durée d'éxécution du traitement
		duree = time.time() - start

		#on écrit les stats dans le fichier log
		log.write("Durée de traitement du corpus : {} secondes.\n".format(timedelta(seconds=round(duree))))
		log.write("Nombre de doublons retirés : {} sur {} contributions du corpus original, soit {}% des contributions.\n".format(compteurDoublon,compteurTotalContrib,round((compteurDoublon/compteurTotalContrib)*100,2)))
		log.write("Nombre de contributions normalisées et nettoyées au niveau de la ponctuation : {} sur {} contributions du corpus original, soit {}% des contributions.\n".format(compteurNormPunct,compteurTotalContrib,round((compteurNormPunct/compteurTotalContrib)*100,2)))
		log.write("Nombre de contributions normalisées et nettoyées au niveau des majuscules : {} sur {} contributions du corpus original, soit {}% des contributions.\n".format(compteurNormMaj,compteurTotalContrib,round((compteurNormMaj/compteurTotalContrib)*100,2)))

		print("Le fichier XML a été généré dans votre répertoire courant, son nom est : {}\nUn fichier log a également été généré.".format(outFileName))

if __name__ == '__main__':
  main(sys.argv[1])