#!/bin/bash

cat GDN-sample.txt.tt | cut -f 1 > toks.txt
codes=$(cat benchmark-EN.csv | cut -d "," -f 2 | tail -n +2)
for dev in $codes; do
	if [ ! -f GDN-sample.txt.tt.$dev ]; then
		cp GDN-sample.txt.tt GDN-sample.txt.tt.$dev
	fi
	ls GDN-sample.txt.tt.$dev;
	cat GDN-sample.txt.tt.$dev | cut -f 4 > GDN-sample.annots.$dev
done
cat toks.txt > GDN-sample.toks.annots.all
for dev in $codes; do
	cp GDN-sample.toks.annots.all GDN-sample.toks.annots.all.bak
	paste GDN-sample.toks.annots.all.bak GDN-sample.annots.$dev > GDN-sample.toks.annots.all
	rm GDN-sample.toks.annots.all.bak
done
rm toks.txt GDN-sample.annots.*
