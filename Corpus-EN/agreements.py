#!/usr/bin/env python3

import csv
codes = []
code_indexes = {}
with open('benchmark-EN.csv') as csvfile:
	tools = csv.reader(csvfile, delimiter=',')
	next(tools)
	for tool in tools:
		code = tool[1]
		codes.append(code)
		indexesall = tool[2]
		code_indexes[code] = []
		for indexes in indexesall.split('|'):
			if len(indexes):
				indexesint = [int(index) for index in indexes.split('-')]
				if len(indexesint) == 2:
					code_indexes[code].append(indexesint)
					
import itertools
labels = []
annotationspairs = {}
lcount = 0
for l in open('GDN-sample.toks.annots.all'):
	print(l, lcount)
	tokannots = l[:-1].split('\t')
	annotators = []
	annotations = {}
	for code in codes:
		isin = False
		for indexes in code_indexes[code]:
			if lcount >= indexes[0] and lcount <= indexes[1]:
				isin = True
		if isin:
			annotators.append(code)
			label = tokannots[codes.index(code)+1]
			print(codes)
			print(code_indexes)
			print(tokannots, code, codes.index(code)+1, label)
			if not len(label):
				label = 'O'
			if not label in labels:
				labels.append(label)
			annotations[code] = labels.index(label)
	print(annotators, annotations, labels)
	for pair in itertools.combinations(annotators, 2):
		pairsorted = sorted(list(pair))
		pairname = '-'.join(pairsorted)
		if not pairname in annotationspairs:
			annotationspairs[pairname] = []
		annotationspairs[pairname].append([annotations[pairsorted[0]], annotations[pairsorted[1]]])
	lcount +=1

print(labels)
from sklearn.metrics import f1_score
for label in labels:
	print(label)
	labelindex = labels.index(label)
	for pairname in annotationspairs:
		annotationsone = [int(annotationspairlabels[0] == labelindex) for annotationspairlabels in annotationspairs[pairname]]
		annotationssec = [int(annotationspairlabels[1] == labelindex) for annotationspairlabels in annotationspairs[pairname]]
		print(pairname, len(annotationspairs[pairname]), f1_score(annotationsone, annotationssec, average='micro'))
